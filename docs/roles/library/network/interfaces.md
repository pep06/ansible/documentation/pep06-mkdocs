---
title: interfaces  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[network/interfaces.ifupdown]: ../interfaces.ifupdown/
[network/interfaces.freebsd]:  ../interfaces.freebsd/

# Métarôle ``network/interfaces``

Ce rôle permet la configuration des interfaces réseau de l'hôte cible.

## Description

Le métarôle ``network/interfaces`` fournit un point d'entrée unique pour
la configuration des interfaces réseau. Il s'agit d'un rôle virtuel qui
délègue les tâches de configuration à un sous-rôle spécialisé appelé
_fournisseur_.

## Fournisseurs

Ce métarôle prend en charge les fournisseurs suivants:

| Plateforme | Version | Fournisseur associé                                            |
| ---------- | ------: | -------------------------------------------------------------- |
| Debian     |       9 | [``network/interfaces.ifupdown``][network/interfaces.ifupdown] |
| Debian     |      10 | [``network/interfaces.ifupdown``][network/interfaces.ifupdown] |
| FreeBSD    |      10 | [``network/interfaces.freebsd``][network/interfaces.freebsd]   |
| FreeBSD    |      11 | [``network/interfaces.freebsd``][network/interfaces.freebsd]   |

## Dépendances

Ce rôle n'a aucune dépendance particulière.

## Paramètres d'invocation

L'exécution de ce rôle peut être personnalisée au moyen de paramètres
d'invocation. Ces paramètres doivent être définis à l'intérieur de la
variable ``role__network_interfaces__params`` au moment de l'invocation
du rôle par le playbook.

````yaml
---
role__network_interfaces__params:
  provider: <str?>
````

### ``provider`` (**str**, _optionnel_)

Cette clé permet de spécifier explicitement le fournisseur que le
métarôle devra appeler. Si cette clé n'est pas définie, le métarôle
choisira lui-même le fournisseur le plus adapté à la plateforme cible.
Cette clé peut prendre l'une des valeurs suivantes:

| Valeur       | Fournisseur à utiliser                                         |
| ------------ | -------------------------------------------------------------- |
| ``freebsd``  | [``network/interfaces.freebsd``][network/interfaces.freebsd]   |
| ``ifupdown`` | [``network/interfaces.ifupdown``][network/interfaces.ifupdown] |
