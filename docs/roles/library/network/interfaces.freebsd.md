---
title: interfaces.freebsd  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[my_network_addresses]:   /inventory/variables/library/my/my_network_addresses
[my_network_gateways]:    /inventory/variables/library/my/my_network_gateways
[my_network_interfaces]:  /inventory/variables/library/my/my_network_interfaces
[my_network_routes]:      /inventory/variables/library/my/my_network_routes

[network/interfaces]: ../interfaces/

# Fournisseur ``network/interfaces.freebsd``

Ce rôle effectue la configuration des interfaces réseau sur un hôte
FreeBSD.

## Description

Le fournisseur ``network/interfaces.freebsd`` effectue la configuration
des interfaces réseau au moyen du framework _bsdinit_ de FreeBSD. Il
génère la configuration réseau sous la forme de variables _rc_, définies
dans des fichiers stockés dans le répertoire ``/etc/rc.conf.d/``.

En tant que fournisseur, l'invocation de ce rôle n'est jamais directe,
mais effectuée par l'intermédiaire du métarôle [``network/interfaces``]
[network/interfaces].

## Dépendances

### Variables d'inventaire

Ce rôle génère la configuration des interfaces réseau à partir des
données présentes dans l'inventaire. Par conséquent, il nécessite que
les variables d'inventaire suivantes soient définies:

| Variable                                           | Description                           |
| -------------------------------------------------- | ------------------------------------- |
| [``my_network_addresses``][my_network_addresses]   | Liste des adresses IP de l'hôte       |
| [``my_network_gateways``][my_network_gateways]     | Liste des passerelles IP de l'hôte    |
| [``my_network_interfaces``][my_network_interfaces] | Liste des interfaces réseau de l'hôte |
| [``my_network_routes``][my_network_routes]         | Liste des routes IP de l'hôte         |

## Paramètres d'invocation

Ce rôle ne supporte aucun paramètre d'invocation particulier.
