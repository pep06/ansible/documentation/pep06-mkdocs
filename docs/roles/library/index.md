---
title: Vue d'ensemble  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

# Vue d'ensemble de la bibliothèque de rôles

Cette section contient des pages de documentation individuelles pour
chacun des rôles maintenus par l'association.

## ``network`` : Configuration réseau

[network/interfaces]:           network/interfaces/
[network/interfaces.freebsd]:   network/interfaces.freebsd/
[network/interfaces.ifupdown]:  network/interfaces.ifupdown/

| Rôle                                                           | Type        | Description                                      |
| -------------------------------------------------------------- | ----------- | ------------------------------------------------ |
| [``network/interfaces``][network/interfaces]                   | Métarôle    | Configuration des interfaces réseau              |
| [``network/interfaces.freebsd``][network/interfaces.freebsd]   | Fournisseur | Configuration des interfaces réseau sur FreeBSD  |
| [``network/interfaces.ifupdown``][network/interfaces.ifupdown] | Fournisseur | Configuration des interfaces réseau via ifupdown |