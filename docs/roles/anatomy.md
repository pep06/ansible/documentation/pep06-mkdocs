---
title: Anatomie  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

# Anatomie des rôles

Les rôles maintenus par l'association partagent une anatomie commune,
que cette page va tenter de décrire aussi précisément que possible.
Il est attendu que le lecteur soit déjà familier des rôles Ansible
avant de consulter cette page.

## Éléments obligatoires

Les rôles que nous maintenons doivent au minimum intégrer les éléments
obligatoires suivants:

| Emplacement           | Description                             |
|-----------------------|-----------------------------------------|
| ``defaults/main.yml`` | Valeurs de configuration par défaut     |
| ``shared/``           | Tâches partagées (métarôles uniquement) |
| ``tasks/init.yml``    | Tâches d'initialisation                 |
| ``tasks/main.yml``    | Point d'entrée principal                |
| ``vars/meta.yml``     | Métavariable de rôle                    |

### ``defaults/main.yml``

Le fichier ``defaults/main.yml`` stocke les valeurs de configuration par
défaut du rôle. Ce fichier est automatiquement chargé lors de l'appel
du rôle, et les variables qu'il contient sont initialisées à leurs
valeurs par défaut à moins qu'elles n'aient été surchargées en amont
de l'invocation. Tout ceci correspond au fonctionnement classique
d'Ansible.

### ``shared/``

Les métarôles doivent posséder un répertoire ``shared`` destiné à
contenir des fichiers de tâches partagées avec leurs fournisseurs. Ce
répertoire doit exister, même vide.

### ``tasks/init.yml``

Le fichier ``tasks/init.yml`` contient les tâches nécessaires à
l'initialisation du rôle, qui incluent le chargement des métavariables,
la fusion des valeurs par défaut des paramètres du rôle avec celles
fournies par le playbook, et diverses vérifications effectuées avant
l'exécution des tâches de configuration.

### ``tasks/main.yml``

Le fichier de tâches ``tasks/main.yml`` correspond au point d'entrée
principal du rôle. Il est nécessaire au fonctionnement d'Ansible. Selon
nos conventions, ne contient aucune tâche de configuration proprement
dite, mais uniquement des tâches d'importation d'autres fichiers de
tâches (``import_tasks``) ou d'autres rôles (``import_role``).

### ``vars/meta.yml``

Le fichier ``vars/meta.yml`` est utilisé pour définir une seule variable
spéciale, de type ``dict``, connue comme la **métavariable de rôle**.
Cette variable décrit les caractéristiques du rôle courant, telles
que les plateformes supportées et des paramètres de configuration
adaptés à chaque plateforme.

## Éléments facultatifs

En plus des éléments obligatoires, des éléments supplémentaires tels que
des fichiers ou des modèles peuvent être ajoutés aux rôles qui les
nécessitent. Lorsqu'ils sont présents, ces éléments doivent être
enregistrés aux emplacements suivants:

| Emplacement                   | Description    |
|-------------------------------|----------------|
| ``templates/<name>.<ext>.j2`` | Modèles Jinja2 |
