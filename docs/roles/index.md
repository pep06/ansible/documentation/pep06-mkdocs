---
title: Vue d'ensemble  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[repos-roles]: /repos/roles/
[anchor-role_categories]: #categories-de-roles

[bb-ansible]: https://bitbucket.org/account/user/pep06/projects/ANSIBLE
[bb-sshsrv]: https://bitbucket.org/pep06/ansible-role-rshell-openssh_server
[bb-apache]: https://bitbucket.org/pep06/ansible-role-www-apache

[gitlab_roles]:  https://gitlab.com/pep06/ansible/roles
[gitlab_roles_network_interfaces_ifupdown]: https://gitlab.com/pep06/ansible/roles/network/interfaces.ifupdown

# Vue d'ensemble des rôles Ansible

## Introduction
Ansible permet de rassembler les ressources nécessaires au déploiement
d'une configuration sous la forme d'unités appelées _rôles_. Ces rôles
sont réutilisables et publiables indépendamment des inventaires.

### Obligation d'emploi
Dans l'absolu, l'usage de rôles n'est pas obligatoire: il est en effet
possible d'employer Ansible avec des playbooks ou des listes de tâches
volantes. C'est une pratique que nous nous interdisons en-dehors de
quelques cas particuliers qui seront explicités plus bas. En d'autres
termes, **le recours aux rôles est obligatoire**.

### Métarôles et fournisseurs
Lorsque plusieurs implémentations d'une même technologie doivent être
déployées, notre politique est de maintenir un rôle dédié à chaque
implémentation. Afin de maintenir un haut niveau d'intégration, chacun
de ces rôles est néanmoins tenu de respecter une interface commune en
termes de noms de variables et de paramétrage. Cette interface est
représentée par un rôle virtuel appelé _métarôle_. Les sous-rôles
correspondant à chaque implémentation sont appelés _fournisseurs_.

???+ example "Exemple de métarôle"
    Grâce aux types de données, nos inventaires sont en capacité de
    représenter la configuration réseau de leurs hôtes d'une manière
    uniforme, quelle que soit la plateforme cible. Il revient donc aux
    rôles de traduire cette représentation en une configuration adaptée
    à la plateforme finale.

    Nous considérons donc qu'une même configuration réseau peut être
    _implémentée_ de plusieurs manières différentes selon la plateforme
    cible. Par conséquent, nous utilisons un rôle Ansible pour chaque
    implémentation particulière, notamment:

      - ``network/interfaces.rh-ifcfg`` pour les plateformes utilisant
        le framework _rh-ifcfg_, à savoir les dérivées de RHEL

      - ``network/interfaces.ifupdown`` pour les plateformes utilisant
        le framework _ifupdown_, à savoir les dérivées de Debian
      
      - ``network/interfaces.freebsd`` pour FreeBSD
    
    Chacun de ces rôles est considéré comme un _fournisseur_ de
    configuration réseau. Un métarôle ``network/interfaces`` sert quant
    à lui de boîte de dérivation: plutôt que d'invoquer directement le
    fournisseur d'une implémentation spécifique, l'on invoquera plutôt
    le métarôle en le laissant sélectionner un fournisseur adapté à la
    plateforme cible.

Cette organisation nous permet de maintenir chaque implémentation dans
un rôle séparé, ce qui est un gage de confort et de sécurité. En outre,
le recours aux métarôles nous permet de maintenir des playbooks
génériques, applicables à plusieurs plateformes ou implémentations.

## Catégories de rôles
Afin de faciliter la gestion des rôles et de limiter les risques de
collision de noms de variables, chaque rôle utilisé par l'association
doit obligatoirement se voir assigner une des catégories suivantes:

| Catégorie    | Objet des rôles membres                                    |
| ------------ | ---------------------------------------------------------- |
| ``backup``   | Services de sauvegardes                                    |
| ``db``       | Services de bases de données                               |
| ``lang``     | Langages de balisage et de programmation                   |
| ``network``  | Périphériques et sous-systèmes réseau                      |
| ``netfs``    | Systèmes de fichiers réseau                                |
| ``platform`` | Plateformes dotées d'API spécifiques                       |
| ``rshell``   | Services de sessions distantes                             |
| ``storage``  | Périphériques de stockage, volumes et systèmes de fichiers |
| ``sys``      | Sous-systèmes critiques liés au noyau                      |
| ``webapp``   | Applications web                                           |
| ``www``      | Services web                                               |

???+ example "Exemples de rôles et de leurs catégories"
    - Le rôle chargé du déploiement du serveur OpenSSH appartient par
      exemple à la catégorie ``rshell``, car SSH est un protocole
      servant à l'établissement de sessions distantes.

    - Le rôle chargé du déploiement du serveur web Apache appartient à
      la catégorie ``www`` car Apache est un serveur web.

## Nommage des rôles
Chaque rôle doit porter un nom unique formé à partir de l'identifiant
de sa catégorie et du nom du service déployé. Ce nom doit être formé
à partir du patron suivant:

```perl
<category>/<role_name>[_server|_client][.<provider>]
```

Où:

  - ``<category>`` est un des identifiants de catégories donnés dans le
    tableau de la section précédente.
  
  - ``role_name`` est le nom du rôle. Ce nom peut-être augmenté, s'il y
    a lieu, d'un suffixe indiquant la partie déployée:
      - ``_server`` si le rôle déploie uniquement la partie serveur
      - ``_client`` si le rôle déploie uniquement la partie cliente
  
  - Le suffixe optionnel ``.<provider>`` ne sera présent que dans les
    noms de rôles assumant la fonction de _fournisseur_ d'un métarôle.
    Il indique le nom de l'implémentation gérée par le fournisseur.

???+ example "Exemples de noms de rôles"
    - Le rôle chargé du déploiement du serveur OpenSSH, qui appartient
      à la catégorie ``rshell``, sera nommé ``rshell/openssh_server``.
    
    - En revanche, celui chargé du déploiement du client OpenSSH sera
      nommé ``rshell/openssh_client``.

    - Le rôle chargé du déploiement du serveur web Apache, qui
      appartient à la catégorie ``www`` sera nommé ``www/apache``.
      Apache n'existant qu'en version serveur, le suffixe ``_server``
      est omis car superflu.
    
    - Le rôle chargé de configurer le chiffrement du stockage sera
      nommé ``storage/encryption``. Il s'agit d'un métarôle possédant
      plusieurs fournisseurs. Celui gérant le framework GELI de FreeBSD
      sera par exemple nommé ``storage/encryption.geli``.

## Dépôts de rôles
Chaque rôle utilisé par l'association est soumis au contrôle de version
via le logiciel _Git_. À l'origine, les dépôts autoritaires étaient
hébergés chez notre prestaire _Bitbucket_ et n'étaient pas accessibles
publiquement. Nous sommes en train de migrer progressivement nos dépôts
vers _GitLab_, en privilégiant autant que possible une accessibilité
publique. Pour plus d'informations, consultez la [section consacrée
aux dépôts de rôles][repos-roles].

## Chemin d'accès
Les dépôts de rôles sont référencés par les dépôts d'espaces de travail
sous la forme de sous-modules _git_, et stockés à l'intérieur du
sous-répertoire ``roles``. À l'intérieur de ce répertoire, les rôles
sont organisés par catégories, à raison d'un sous-répertoire par
catégorie de rôle.

???+ example "Exemple d'emplacements de rôles dans l'arborescence"
    - Le sous-module _git_ correspondant au rôle
      ``rshell/openssh_server`` sera disponible à l'emplacement
      ``roles/rshell/openssh_server`` du dépôt d'espace de travail.

    - Le sous-module _git_ correspondant au rôle ``www/apache`` sera
      disponible à l'emplacement ``roles/www/apache`` du dépôt
      d'espace de travail.

## Utilisation des rôles
### Invocation simple
Les rôles sont invoqués depuis les playbooks en spécifiant leur chemin
d'accès relatif à partir de l'espace de travail Ansible.

???+ example "Exemple d'invocation d'un rôle Ansible"
    L'extrait de playbook suivant invoque le rôle ``network/interfaces``
    pour tous les hôtes de l'inventaire:

    ````yaml
    ---
    - name: Configuration commune à tous les hôtes
      hosts: ['all']
      roles:
        - role: network/interfaces
          tags: ['role::network-interfaces']
    ````

### Tags
Comme le montre l'exemple ci-dessus, il est d'usage de définir un
tag pour chaque invocation de rôle. Ce tag, qui est construit sur le
modèle ``role::<role_category>-<role_name>``, permet de limiter
l'exécution d'un playbook à un ou plusieurs rôles:

???+ example "Exemple d'invocation d'un playbook avec filtre sur les rôles"
    L'extrait de code suivant invoquera le playbook ``test.yml`` en
    incluant uniquement le rôle ``network/interfaces``:

    ````bash
    ansible-playbook \
        -i inventories/pep06 \
        -t role::network-interfaces \
        playbooks/pep06/test.yml
    ````

### Invocation avec paramètres
Certains rôles peuvent être invoqués avec des paramètres personnalisés,
qui permettent de modifier certains aspects de leur exécution. Pour ce
faire, les paramètres à passer au rôle doivent être définis au sein
d'une clé supplémentaire ajoutée au bloc d'invocation. Cette clé doit
être nommée sur le modèle suivant:

````perl
role__<role_category>_<role_name>__params
````

???+ example "Exemple d'invocation paramétrée d'un rôle Ansible"
    L'extrait de playbook suivant invoque le rôle ``storage/encryption``
    avec un jeu de paramètres personnalisés:

    ````yaml
    ---
    - name: Configuration commune à tous les hôtes
      hosts: ['all']
      roles:
        - role: storage/encryption
          tags: ['role::storage-encryption']
          role__storage_encryption__params:
            enable_trim: true
    ````

Au moment de son invocation, le rôle fusionnera automatiquement ses
valeurs de configuration par défaut avec celles fournies par le
playbook.