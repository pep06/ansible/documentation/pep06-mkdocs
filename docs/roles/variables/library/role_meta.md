---
title: role_meta  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[role_platform_options]: ../role_platform_options/

# Variable ``role_meta`` (**dict**, _obligatoire_)

## Description

???+ info "Définition obligatoire"
    Cette variable doit obligatoirement être définie par chaque rôle.

La variable spéciale ``role_meta`` contient les métadonnées concernant
le rôle, ainsi que ses valeurs de configuration non personnalisables.

## Valeur par défaut

Cette variable n'a pas de valeur par défaut. Elle doit obligatoirement
être définie par chaque rôle.

## Structure attendue

La variable ``role_meta`` doit satisfaire à la structure suivante:

````yaml
---
role_meta:
  supported_virtualization:
    host: <str[]>
    guest: <str[]>
    none: <str[]>
  supported_platforms: <dict>
````

### Clés communes

Les clés documentées dans cette section sont applicables à n'importe
quel rôle.

#### ``supported_virtualization.host`` (**str[]**, _obligatoire_)

Cette clé permet de définir la liste des types de virtualisation
supportés par le rôle lorsque l'hôte distant est un hôte de
virtualisation, c'est-à-dire quand la variable
``ansible_virtualization_role`` vaut ``host``.

Cette clé peut être utilisée de différentes manières:

- Si cette clé référence une liste vide, le rôle sera réputé ne pas
  supporter l'exécution sur les hôtes de virtualisation.
  
- Si cette clé contient l'élément spécial ``any``, le rôle sera réputé
  supporter l'exécution sur n'importe quel type d'hôte de
  virtualisation.

- Si cette clé contient des éléments autres que ``any``, le rôle se
  chargera de vérifier que le type de virtualisation fourni par l'hôte
  distant (représenté par la variable ``ansible_virtualization_type``)
  fait partie de cette liste.

#### ``supported_virtualization.guest`` (**str[]**, _obligatoire_)

Cette clé permet de définir la liste des types de virtualisation
supportés par le rôle lorsque l'hôte distant est virtualisé,
c'est-à-dire quand la variable ``ansible_virtualization_role`` vaut
``guest``.

Cette clé peut être utilisée de différentes manières:

- Si cette clé référence une liste vide, le rôle sera réputé ne pas
  supporter l'exécution en environnement virtualisé.
  
- Si cette clé contient l'élément spécial ``any``, le rôle sera réputé
  supporter l'exécution dans n'importe quel type d'environnement
  virtualisé.

- Si cette clé contient des éléments autres que ``any``, le rôle se
  chargera de vérifier que le type de virtualisation utilisé sur l'hôte
  distant (représenté par la variable ``ansible_virtualization_type``)
  fait partie de cette liste.

#### ``supported_virtualization.none`` (**str[]**, _obligatoire_)

Cette clé permet de spécifier que le rôle supporte l'exécution sur les
hôtes qui ne sont ni virtualisés, ni hôtes de virtualisation,
c'est-à-dire lorsque la variable ``ansible_virtualization_role`` vaut
``none``.

Cette clé peut être utilisée de différentes manières:

- Si cette clé contient l'élément spécial ``any``, le rôle sera réputé
  supporter l'exécution en l'absence d'environnement de virtualisation.

- Si cette clé référence toute autre valeur ou une liste vide, le rôle
  sera réputé ne pas supporter l'exécution en l'absence d'environnement
  de virtualisation.

#### ``supported_platforms`` (**dict**, _obligatoire_)

La clé ``supported_platforms`` permet de définir la liste des
plateformes distantes supportées par le rôle. Chaque plateforme est
définie par un couple de valeurs:

  1. Un nom de distribution, correspondant à la valeur de la variable
    ``ansible_distribution`` convertie en lettres minuscules.

  2. Un numéro de version majeure, correspondant à la valeur de la
     variable ``ansible_distribution_major_version`` castée en chaîne.

La structure attendue du dictionnaire référencé par la clé
``supported_platforms`` est donnée ci-dessous:

````yaml
---
supported_platforms:
  "<ansible_distribution | lower()>":
    "<ansible_distribution_major_version>": <dict?>
````

Le dictionnaire occupant l'élément feuille permet de définir des options
spécifiques à la plateforme, et sera utilisé pour initialiser la
variable [``role_platform_options``][role_platform_options]

## Exemples

Aucun pour le moment
