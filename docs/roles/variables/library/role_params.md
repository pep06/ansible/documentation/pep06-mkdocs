---
title: role_params  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[role_defaults]: ../role_defaults/

# Variable ``role_params`` (**dict**, _optionnelle_)

## Description

???+ info "Définition manuelle"
    Cette variable peut être définie manuellement au moment de
    l'invocation d'un rôle.

La variable spéciale ``role_params`` contient les options d'invocation
personnalisées définies par le consommateur du rôle au moment de son
invocation, au niveau du playbook ou du fichier de tâches.

Lorsque cette variable est définie, les valeurs qu'elles contient seront
substituées à celles définies par défaut dans la variable
``role_defaults``. Les valeurs finales seront assignées à la
variable ``role_options``, qui sera la seule à être consommée
par le reste du rôle.

## Valeur par défaut

Cette variable n'a pas de valeur par défaut. Si elle n'est pas définie
au moment de l'invocation du rôle, le rôle utilisera les valeurs par
défaut issues de [``role_defaults``][role_defaults].

## Valeur personnalisée

Cette variable peut prendre n'importe quelle valeur personnalisée.

## Contraintes

La structure du ``dict`` référencé par cette variable doit épouser celle
de la variable [``role_defaults``][role_defaults] du rôle concerné.

## Exemples

Aucun pour le moment
