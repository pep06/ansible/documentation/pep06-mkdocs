---
title: role_options  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[role_defaults]:  ../role_defaults/
[role_params]:    ../role_params/

# Variable ``role_options`` (**dict**, _automatique_)

## Description

???+ info "Définition automatique"
    Cette variable est définie automatiquement par les tâches
    d'initialisation des rôles.

La variable spéciale ``role_options`` contient les valeurs finales des
options d'invocation du rôle. Cette variable est générée dynamiquement
lors de l'initialisation du rôle, et résulte de la fusion des valeurs
d'options par défaut, issues de la variable
[``role_defaults``][role_defaults], et des valeurs personnalisées
passées au rôle via la variable [``role_params``][role_params], lorsque
celle-ci est définie.

Cette variable constitue la seule source autoritaire en matière
d'options d'invocation: l'ensemble des tâches ayant besoin d'accéder à
ces options doivent le faire par son intermédiaire de cette variable. Les
variables [``role_defaults``][role_defaults] et
[``role_params``][role_params] ne doivent jamais être accédées par les
tâches de rôles, et sont considérées comme obsolètes une fois le rôle
initialisé.

## Valeur par défaut

La valeur par défaut de cette variable est identique à celle de la
variable [``role_defaults``][role_defaults].

## Valeur personnalisée

Les valeurs par défaut issues de [``role_defaults``][role_defaults]
peuvent être surchargées en définissant une variable
[``role_params``][role_params] au moment de l'invocation du rôle.

## Contraintes

La structure des variables [``role_defaults``][role_defaults] et
[``role_params``][role_params] doit évidemment être identique afin que
les dictionnaires puissent être fusionnés. La variable ``role_options``
résultant de la fusion possèdera la même structure.

## Exemples

Aucun pour le moment
