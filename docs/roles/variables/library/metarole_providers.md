---
title: metarole_providers  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

# Variable ``metarole_providers``  (**str[]**, _automatique_)

## Description

???+ info "Définition automatique"
    Cette variable est définie automatiquement par les tâches
    d'initialisation des métarôles.

La variable de rôle spéciale ``metarole_provider`` contient la liste
des noms des fournisseurs qui seront invoqués par le métarôle. Cette
liste est générée dynamiquement lors de l'initialisation d'un métarôle

## Valeur par défaut

La valeur par défaut de cette variable correspond à la liste des
fournisseurs par défaut pour la plateforme cible, stockée dans la
variable ``role_platform_options['providers']['default']``.

## Valeur personnalisée

Il est possible de définir une liste personnalisée de fournisseurs à
invoquer via les options d'invocation du rôle. La liste des fournisseurs
doit alors être définie via la variable ``role_params['providers']``.
Toute valeur définie ici surchargera la valeur par défaut.

## Contraintes

Cette liste doit contenir des noms de fournisseurs valides dans le
contexte du métarôle.

## Exemples

Aucun pour le moment.
