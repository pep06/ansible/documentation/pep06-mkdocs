---
title: metarole_shared_tasks  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

# Variable ``metarole_shared_tasks``  (**str**, _automatique_)

## Description

???+ info "Définition automatique"
    Cette variable est définie automatiquement par les tâches
    d'initialisation des métarôles.

La variable de rôle spéciale ``metarole_shared_tasks`` contient le
chemin d'accès complet au répertoire contenant les tâches partagées par
le métarôle avec ses fournisseurs. Cette variable est définie lors de
l'initialisation du métarôle, et consommée par ses fournisseurs
lorsqu'ils ont besoin de faire appel à un fichier de tâches partagées.

## Valeur par défaut

Cette variable est initialisée automatiquement avec le chemin d'accès
à un sous-répertoire ``shared`` dans le répertoire racine du métarôle.

## Valeur personnalisée

Cette variable ne supporte aucune valeur personnalisée.

## Contraintes

La définition de cette variable est obligatoire et automatique, même si
le métarôle ne contient aucun fichier de tâches partagées.

## Exemples

Une définition standard de cette variable est donnée pour référence
ci-dessous:

````yaml
---
- name: Define the path to shared task files
  set_fact:
    metarole_shared_tasks: "{{ role_path ~ '/shared' }}"
````