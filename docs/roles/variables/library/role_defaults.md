---
title: role_defaults  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[metarole_providers]:   ../metarole_providers/
[role_options]:         ../role_options/
[role_params]:          ../role_params/

# Variable ``role_defaults`` (**dict**, _obligatoire_)

## Description

???+ info "Définition obligatoire"
    Cette variable doit obligatoirement être définie par chaque rôle,
    quitte à référencer une valeur nulle.

La variable spéciale ``role_defaults`` définit les valeurs par défaut
des options d'invocation du rôle. Ces valeurs sont utilisées par
défaut lorsque le rôle est invoqué sans qu'une variable
[``role_params``][role_params] n'ait été définie.

Cette variable est définie dans le fichier ``defaults/main.yml``. Sa
déclaration est obligatoire, même si le rôle n'utilise aucune option.
Elle peut par conséquent référencer une valeur vide.

Cette variable sera chargée automatiquement lors de l'invocation du
rôle. Les valeurs qu'elle contient seront utilisées par défaut pour
définir la variable ``role_options`` dans le cadre des tâches
d'initialisation du rôle, à moins que ces-dernières n'aient été
surchargées dans la variable ``role_params``.

## Valeur par défaut

Cette variable n'a pas de valeur par défaut. Elle doit obligatoirement
être définie par chaque rôle.

## Valeur personnalisée

Chaque rôle est libre de structurer comme il le souhaite les données
référencées par cette variable.

## Contraintes

### Identité de structure

La structure des variables [``role_defaults``][role_defaults] et
[``role_params``][role_params] doit évidemment être identique afin que
les dictionnaires puissent être fusionnés pour générer la variable
[``role_options``][role_options].

### Possibilité de surcharge

Les valeurs de configuration définies dans la variable ``role_defaults``
ont vocation à pouvoir être surchargés par le consommateur du rôle.
Il est donc impératif d'éviter d'y inclure des valeurs que l'on ne
souhaite pas voir modifiées lors de l'invocation. De telles valeurs
doivent être définies en tant qu'options de plateforme, via la variable
[``role_platform_options``][role_platform_options].

## Structure attendue

Les données référencées par cette variable peuvent être structurées
librement, à condition de satisfaire aux quelques contraintes suivantes:

````yaml
---
# Clés uniquement applicables aux métarôles
provider: <str[]?>
````

### Clés uniquement applicables aux métarôles

Les clés documentées dans cette section sont uniquement applicables
aux variables ``role_defaults`` issues de métarôles.

#### ``provider`` (**str[]**, _optionnel_)

La clé ``providers``, qui est optionnelle pour les rôles classiques mais
obligatoire pour les métarôles, permet de définir statiquement la liste
des fournisseurs qui seront invoqués par le métarôle, au lieu de
s'appuyer sur le mécanisme de sélection automatique basé sur la nature
de la plateforme distante.

Dans le contexte des options d'invocation par défaut d'un métarôle, il
est attendu que cette clé soit simplement initialisée avec une liste
vide (``[]``), afin de faciliter les opérations de fusion avec une clé
de même nom issue de [``role_params``][role_params].

Si la valeur de cette clé dans [``role_options``][role_options] est
toujours celle d'une liste vide, les options de plateformes seront
utilisées pour déterminer la liste des fournisseurs à utiliser, qui sera
stockée dans [``metarole_providers``][metarole_providers]. Si, en
revanche, cette liste contient au moins une entrée, les options de
plateformes seront ignorées, et la variable [``metarole_providers``]
[metarole_providers] sera initialisée directement à partir de sa valeur.

## Exemples

Aucun pour le moment
