---
title: role_platform_options  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[storage.encryption.options]: /types/library/fr.pep06.schemas/storage.encryption.options

# Variable ``role_platform_options``   (**dict**, _automatique_)

## Description

???+ info "Définition automatique"
    Cette variable est définie automatiquement par les tâches
    d'initialisation des rôles.

La variable de rôle spéciale ``metarole_provider`` contient des
paramètres de configuration non-modifiables spécifiques à la plateforme
cible. Cette variable est déclarée automatiquement au moment de
l'initialisation du rôle, et se voit assigner un dictionnaire issu
de la métavariable de rôle ``role_meta``, contenant des informations
de configuration adaptées à la plateforme où le rôle est amené à opérer.

Les options de plateforme sont utilisées lorsqu'il est nécessaire
d'adapter l'exécution du rôle au contexte où il opère, mais que l'on
ne souhaite pas laisser la possibilité à l'utilisateur de surcharger
les valeurs de configuration au moyen de la variable ``role_params``.

## Valeur par défaut

La valeur par défaut de cette variable correspond au contenu de la
clé ``role_meta['supported_distributions'][distribution][version]``, où:

- ``distribution`` est la valeur de ``ansible_distribution`` sur l'hôte
  cible, convertie en caractères minuscules

- ``version`` est la valeur de ``ansible_distribution_major_version``
  sur l'hôte cible, convertie en chaîne

Ainsi, pour un hôte tournant sous CentOS 7, la valeur par défaut de
cette variable sera initialisée à partir du contenu de la clé
``role_meta['supported_distributions']['centos']['7']``.

## Valeur personnalisée

Contrairement aux options d'invocation les options de plateforme ne sont
pas personnalisables.

## Contraintes

Cette variable est obligatoirement et automatiquement définie lors de
l'initialisation d'un rôle, même s'il n'existe aucune option définie
pour la plateforme cible.

## Exemples

???+ example "Exemple d'utilisation des options de plateforme"
    Le rôle ``storage/encryption.geli`` utilise des options de
    plateforme pour définir diverses constantes nécessaires à la bonne
    exécution du rôle, telles que la traduction des identifiants des
    d'algorithmes de chiffrement supportés par le type de données
    [``storage.encryption.options``][storage.encryption.options] en
    identifiants d'algorithmes compréhensibles par GELI.

    La métavariable de rôle définie dans ``vars/meta.yml`` vaut:

    ````yaml
    ---
    role_meta:
      supported_virtualization:
        host: ['any']
        guest: ['any']
        none: ['any']
      supported_distributions:
        defaults: &defaults
          supported_schemes: ['gpt', 'mbr']
          mappings:
            algorithms:
              authentication:
                none: "NULL"
                md5: HMAC/MD5
                sha1: HMAC/SHA1
                sha256: HMAC/SHA256
                sha512: HMAC/SHA512
              encryption:
                none: "NULL"
                aes-cbc-plain: AES-CBC
                aes-xts-plain: AES-XTS
        freebsd:
          defaults: &freebsd_defaults
            <<: *defaults
          "11": *freebsd_defaults
          "12": *freebsd_defaults
    ````

    Sur une plateforme FreeBSD 11 ou 12, qui sont les seules supportées,
    la variable ``role_platform_options`` vaudra:

    ````yaml
    ---
    role_platform_options:
      supported_schemes: ['gpt', 'mbr']
      mappings:
        algorithms:
          authentication:
            none: "NULL"
            md5: HMAC/MD5
            sha1: HMAC/SHA1
            sha256: HMAC/SHA256
            sha512: HMAC/SHA512
          encryption:
            none: "NULL"
            aes-cbc-plain: AES-CBC
            aes-xts-plain: AES-XTS
    ````