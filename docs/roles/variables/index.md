---
title: Vue d'ensemble  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[inventory-variables]: /inventory/variables/

# Vue d'ensemble des variables de rôles

Les rôles que nous maintenons font un usage important des variables,
soit qu'ils consomment des [variables d'inventaire][inventory-variables]
existantes, soit qu'ils en définissent de nouvelles pour leur usage
propre. Cette page s'intéresse aux **variables de rôles**, c'est-à-dire
aux variables qui sont définies par et pour l'usage d'un rôle
spécifique. Ces variables font l'objet de normes que cette page tentera
de décrire.

## Généralités

### Types de variables

Les variables de rôles peuvent être réparties en plusieurs catégories:

  1. Les **variables locales** sont des variables définies à
     discrétion par un rôle pour ses propres besoins. L'usage de ces
     variables est libre et faiblement normé.

  2. Les **variables spéciales** sont des variables dont le nom et
     la fonction sont prédéfinis, que chaque rôle peut ou doit définir.

### Durée de vie

La durée de vie effective des variables de rôles varie selon leur type:

- Les variables définies lors de l'invocation du rôle persisteront
  jusqu'à la fin de l'invocation courante

- Les _facts_ Ansible persisteront au-delà de la boucle ou du rôle
  courant, jusqu'à la fin de l'exécution de l'unité de playbook.

- Les variables de boucles ont une durée de vie limitée à l'itération
  courante.

Au-delà de ces particularismes, l'usage de nos variables de rôles repose
sur les principes suivants:

- Les variables locales ont une durée de vie limitée à l'invocation
  courante

- Les variables spéciales débutant par ``role_`` ont une durée de vie
  limitée à l'invocation courante

- Les variables spéciales débutant par ``metarole_`` ont une durée de
  vie limitée à la chaîne d'invocation initiée par le métarôle.

## Variables locales

### Définition

Les variables locales sont des variables définies à discrétion par
un rôle pour son usage propre. Cette catégorie de variable regroupe:

- Les _facts_ Ansible définis par un rôle dans le cadre de ses
  opérations à l'aide du module ``set_fact``.

- Les variables de boucles telles que _item_, ou les variables de boucle
  personnalisées définies via ``loop_control/loop_var``.

Il s'agit du type de variables de rôles le plus courant. Par défaut,
toute variable de rôle est réputée locale à moins d'être spéciale.

### Nommage

Le nommage des variables locales est laissé relativement libre
pusiqu'il ne doit obéir qu'à une seule contrainte: les noms de ces
variables doivent impérativement comporter le préfixe double underscore
(``__``).

???+ example "Exemples de variables locales"
    Dans l'extrait de code suivant, les variables ``__partition_start``,
    ``__volume`` et ``__partition_index`` sont des variables locales:

    ````yaml
    - name: Build partition start offset
      set_fact:
        __partition_start: >-
          {%- if 'start' in __volume['partition'] -%}
            {{ __volume['partition']['start'] }}
          {%- else -%}
            {%- if __partition_index | int == 0 -%}
              {{ '0%' }}
            {%- else -%}
              {{ false }}
            {%- endif -%}
          {%- endif -%}
    ````

## Variables spéciales

### Définition

Les variables spéciales sont des variables dont le nom et l'usage sont
prédéfinis, que chaque rôle peut (ou doit) définir. Ces variables sont
employées afin de fournir une implémentation uniforme de fonctionnalités
communes à l'ensemble des rôles, telles que la gestion des des
paramètres d'invocation.

### Nommage

Les variables spéciales sont reconnaissables au premier coup d'oeil en
ce qu'elles débutent toutes par un des préfixes réservés suivant:

| Préfixe       | Description                                |
|---------------|--------------------------------------------|
| ``role_``     | Variable de rôle spéciale à portée locale  |
| ``metarole_`` | Variable de rôle spéciale à portée étendue |

Les variables spéciales en ``role_`` peuvent être définies par n'importe
quel rôle, qu'il soit autonome, métarôle ou fournisseur. Leur durée de
vie est limitée à l'invocation courante.

Les variables spéciales en ``metarole_`` ne peuvent être définies que
par les métarôles. Leur durée de vie est étendue à l'ensemble de la
chaîne d'invocation, c'est-à-dire qu'elles sont définies à la fois pour
le métarôle et les fournisseurs qu'il invoque, avec surcharge locale
si la variable est redéfinie par un fournisseur.
