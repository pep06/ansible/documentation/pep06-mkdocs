---
title: Infrastructures de clés publiques
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[my_pki_ssh_keypairs]:  /inventory/variables/library/my/my_pki_ssh_keypairs
[our_pki_ssh_keypairs]: /inventory/variables/library/our/our_pki_ssh_keypairs
[pki.ssh.keypair]:      /types/library/fr.pep06.schemas/pki.ssh.keypair

# À propos des infrastructures de clés publiques

Les inventaires Ansible supportent la définition de diverses formes de
clés publiques (SSH, PGP, etc.). Ce petit guide donne une vision
d'ensemble de la manière dont nous gérons ces objets dans nos dépôts
Ansible.

## Principes généraux
### Restrictions concernant les clés privées
Pour des raisons de sécurité, et quel que soit le type de clé considéré,
nos dépôts n'intègrent **jamais** la partie privée d'une paire de clés
de chiffrement, même chiffrées via PGP ou Ansible Vault. Seules les clés
publiques peuvent être intégrées à Ansible. Les clés privées ne
doivent jamais quitter les hôtes qui les ont générées, et encore moins
transiter par le réseau.

## Clés SSH
Nos dépôts intègrent des listes de paires de clés SSH au moyen de
variables de type [!fr.pep06.schemas.pki.ssh.keypair[]]
[pki.ssh.keypair].

### Variables
Parce qu'elles sont d'usage courant, les paires de clés SSH peuvent
être définies au moyen de plusieurs variables d'inventaire:

  - **A l'intérieur de la variable d'inventaire publique
    [``our_pki_ssh_keypairs``][our_pki_ssh_keypairs]**  
    Cette variable a vocation à contenir les clés publiques SSH connues
    de tous les hôtes, telles que les celles des administrateurs
    systèmes.

  - **A l'intérieur de la variable d'inventaire privée
    [``my_pki_ssh_keypairs``][my_pki_ssh_keypairs]**  
    Cette variable a vocation à contenir les clés publiques SSH dont la
    publication est réduite à un groupe d'hôtes ou à un hôte unique. Il
    s'agira par exemple des clés publiques des hôtes membres d'un même
    cluster.

### Arborescence
Afin de faciliter l'administration des clés, il est d'usage de ne pas
stocker les clés SSH à l'intérieur des fichiers YAML déclarant les
instances de [``pki.ssh.keypair``][pki.ssh.keypair], mais dans des
fichiers indépendants, rangés dans le même répertoire que les fichiers
YAML et nommés identiquement, à la différence de l'extension:

  - L'extension ``.key`` sera utilisée pour identifier un fichier
    stockant une clé privée, bien que l'inventaire n'en comporte pas,
    cette information étant donnée ici par souci d'exhaustivité.

  - L'extension ``.pub`` sera utilisée pour identifier un fichier
    stockant une clé publique

Les fichiers stockant des clés privées doivent obligatoirement être
chiffrés via PGP ou _Ansible Vault_. Le contenu de ces fichiers externes
sera inclus dynamiquement par Ansible au moment de son invocation via
l'usage de _lookup plugins_, comme l'illustre l'exemple ci-dessous.

???+ example "Illustration du stockage des clés dans des fichiers séparés"
    L'extrait de code ci-dessous déclare une paire de clés portant
    l'uid ``root``. Cette paire de clé sera uniquement accessible par
    l'hôte ``my_host``. Les clés privée et publique sont respectivement
    stockées dans les fichiers ``_root.key`` et ``_root.pub``, localisés
    dans le même répertoire que ``_root.yml``.

    ````yaml
    # <inventory_dir>/host_vars/my_host/my/pki/ssh/keypairs/_root.yml
    ---
    # !fr.pep06.schemas.pki.ssh.keypair[]
    my_pki_ssh_keypairs__root:
      - uid: root
        tags: []
        algo: rsa
        usage: host
        principal: my_host
        private:
          openssh: >-
            {{ lookup('file', inventory_dir ~
                '/host_vars/my_host/my/pki/ssh/keypairs/_root.key.enc')
            }}
        public:
          openssh: >-
            {{ lookup('file', inventory_dir ~
                '/host_vars/my_host/my/pki/ssh/keypairs/_root.pub') }}
    ````