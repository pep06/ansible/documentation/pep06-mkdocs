---
title: Vue d'ensemble    
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[git_pep06]:    keybase://team/adpep06.it.sysops.ansible/secrets-pep06
[pass]:         https://www.passwordstore.org/
[pass_plugin]:  https://docs.ansible.com/ansible/latest/plugins/lookup/passwordstore.html
[system.user]: /types/library/fr.pep06.schemas/system.user

# Vue d'ensemble de la gestion des secrets

## Introduction
Parce qu'il se trouve au coeur de la gestion de la configuration,
Ansible doit pouvoir accéder à un grand nombre d'informations secrètes
telles que des mots de passe, des paires de clés ou des certificats.

L'accès à ces données doit se faire:

  - **De manière transparente**  
    Ansible doit pouvoir obtenir les secrets dont il a besoin sans
    intervention de l'utilisateur, ce qui implique l'usage d'un
    mécanisme de chiffrement transparent.
  
  - **De manière déterministe**  
    Ansible doit savoir exactement où chercher chaque secret, ce qui
    suppose qu'un schéma d'organisation de ces données ait été défini.

  - **De manière sécurisée**  
    L'accès aux secrets doit être soumis à authentification et
    autorisation, le principal vérifié étant l'identité sous laquelle
    Ansible s'exécute, qui correspond à l'utilisateur courant.

## Historique
### ansible-vault
Au début du déploiement d'Ansible, les secrets utilisés par Ansible
étaient chiffrés via l'utilitaire _ansible-vault_, et directement
stockés dans les inventaires, sous la forme de fichiers YAML. Si cette
manière de faire est suffisante dans le cadre de petits déploiements,
elle se heurte à plusieurs limites:

  - **Le chiffrement des secrets repose sur une clé pré-partagée**  
    Cette clé doit être fournie à chaque utilisateur exécutant Ansible,
    et aucun mécanisme n'est prévu pour rechiffrer la totalité des
    secrets en cas de compromission.

  - **Les secrets sont indissociables à l'inventaire**  
    Alors qu'en tant que données sensibles, il devraient faire l'objet
    d'un stockage et d'une gestion séparée. Le stockage des secrets à
    l'intérieur des inventaires augmente la probabilité de voir des
    secrets être stockés en clair par inadvertance.

  - **Les secrets ne sont pas modifiables**  
    ansible-vault ne prévoie aucun mécanisme simple permettant de créer
    ou modifier des secrets. Cela reste une opération manuelle.

### pass
Pour tenter de combler une partie de ces lacunes, nous avons décidé
de migrer les secrets utilisés par Ansible vers un dépôt git séparé,
géré par l'utilitaire [pass][pass].

Cet utilitaire permet le chiffrement des secrets via PGP, et leur
stockage sous forme de fichiers individuels, rangés dans l' arborescence
du système de fichiers. Ansible inclut un [_lookup plugin_][pass_plugin]
permettant d'interroger ou de mettre à jour le magasin de secrets.
Comme cet outil repose sur PGP, il permet le chiffrement de secrets à
l'aide de clés publiques multiples, et fournit une méthode simple de
rechiffrement du magasin en cas de besoin.

### Notes pour le futur
La migration vers PGP constitue une avancée d'un point de vue
pratique, mais pas tellement d'un point de vue sécuritaire. Le
principal point faible, qui est commun à ansible-vault et à pass,
demeure le fait que les états successifs du magasin de secrets sont
publics, bien que chiffrés.

Lorsque les secrets sont rechiffrés, par exemple après un changement
de mot de passe ansible-vault ou une modification de la liste de
clés PGP autorisées, toute personne ayant connaissance du précédent
mot de passe ou possédant une copie de la clé PGP désautorisée
demeurera en possibilité d'accéder aux secrets pourvu qu'il ait
accès à une copie du dépôt correspondant un état antérieur à ces
modifications. Ce genre de copies est particulièrement aisé à
obtenir lorsque l'on utilise un système contrôle de version tel que
_git_.

Cet inconvénient est commun à toute solution de chiffrement opérant
sur un magasin public. Le seul moyen de s'en affranchir serait de
migrer la gestion de secrets vers un coffre-fort externe imposant
des contraintes d'authentification et d'autorisation d'accès. Dans
l'idéal, cela serait FreeIPA Vault, ou HashiCorp Vault.

## A propos des magasins de secrets
Les secrets devant être protégés sont réuunis au sein de **magasins
de secrets**. Un environnement de travail Ansible peut inclure plusieurs
de ces magasins. La convention est de prévoir un magasin par
organisation ou domaine d'administration.

Jusqu'ici, seules les PEP06 ont été concernées par le déploiement
d'Ansible, si bien qu'il n'existe pour le moment qu'un seul magasin de
secrets, portant l'identifiant ``pep06``.

### Contrôle de versions
Chaque magasin est un dépôt _Git_, référencé en tant que sous-module
par le dépôt principal à l'emplacement relatif ``secrets/<uid>``.
Actuellement, l'emplacement ``secrets/`` ne comprend qu'un seul
sous-répertoire ``pep06``, qui correspond au seul magasin de secrets
configuré. Le tableau ci-dessous liste les dépôts de secrets existants:

| Nom Keybase                    | Emplacement du sous-module | 
|--------------------------------|----------------------------|
| [``secrets-pep06``][git_pep06] | ``secrets/pep06``          | 

Contrairement aux autres dépôts _Git_ utilisés avec Ansible, qui sont
synchronisés avec des dépôts distants hébergés chez GitLab ou BitBucket,
les dépôts de secrets se synchronisent exclusivement avec des dépôts
hébergés par Keybase. Ces dépôts, qui implémentent un mécanisme de
chiffrement natif, sont la propriété de l'équipe Keybase
``adpep06.it.sysops.ansible``, à laquelle vous devez appartenir pour
pouvoir y accéder.

Le chiffrement est intégré de manière native, si bien que l'utilisation
d'un _remote_ Keybase sera transparente pourvu que Keybase ait été
correctement configuré sur la station Ansible.

L'utilitaire [pass][pass], qui assume la gestion de secrets, intègre
un mécanisme automatique qui générera automatiquement une transaction
_Git_ après chaque modification apportée au magasin. Par conséquent,
la seule opération restant à la charge de l'utilisateur est de pousser
régulièrement les changements locaux vers Keybase.

### Chiffrement PGP
L'utilitaire [pass][pass] stocke chaque secret sous la forme d'un
fichier individuel chiffré via PGP. Le chiffrement est effectué au moyen
d'une ou plusieurs clés publiques, dont la liste peut être paramétrée
indépendamment pour chaque répertoire, bien que nous n'ayons pour notre
part jamais eu besoin de ce niveau de granularité.

Les clés publiques utilisées pour le chiffrement des secrets doivent
être présentes dans le trousseau GnuPG de l'utilisateur courant. De
plus, si le déchiffrement des secrets est nécessaire, au moins une de
ces clés publiques doit être accompagnée de sa clé privée, évidemment
protégée par un mot de passe. GnuPG se chargera de vous demander le
mot de passe de déchiffrement de manière interactive quand il en aura
besoin.

Aux PEP06, Ansible est toujours déployé en environnement dédié, ce qui
signifie que chaque installation est liée à un ordinateur dont elle ne
bougera jamais. Par conséquent, nous avons fait le choix de nous appuyer
sur l'infrastructure PGP de Keybase pour gérer nos paires de clés PGP.
Chaque paire de clés PGP utilisée pour le chiffrement des secrets
correspond de fait à une instance d'Ansible installée sur un ordinateur
dédié. Dès lors:

  - La paire de clés est générée localement sur cette instance et
    stockée dans le trousseau GnuPG de l'utilisateur.

  - La clé privée ne quittera jamais l'hôte et disparaîtra avec lui.
    Elle reste protégée par un mot de passe fort.

  - La clé publique est poussée vers Keybase, ce qui permet aux autres
    instances Ansible de la récupérer, de l'importer, et optionnellement
    (pour les puristes) de la signer.

Chaque instance utilisant un magasin de secrets doit posséder, dans son
trousseau GnuPG, la totalité des clés publiques des autres instances
Ansible. Ceci est nécessaire afin que [pass][pass] puisse rendre chaque
chaque secret déchiffrable par la totalité des instances.

## Organisation du magasin
Afin qu'Ansible puisse fonctionner de manière autonome et que le
magasin de secrets conserve sa cohérence, il était nécessaire que nous
nous dotions de conventions permettant de déterminer à coup sûr
les chemins d'accès aux différents types de secrets.

### Portée des secrets
Les secrets sont classés en fonction de leur portée, c'est-à-dire de
la taille de leur audience. L'on distingue ainsi:

  - **Les secrets d'hôtes**  
    Ces secrets sont uniquement accessibles par l'hôte Ansible auquel
    ils appartiennent. Il peut s'agir, par exemple, de la partie privée
    de la paire de clés SSH d'un hôte, ou bien du mot de passe de son
    compte _root_.

  - **Les secrets de groupes**  
    Ces secrets sont accessibles par l'ensemble des membres d'un groupe
    Ansible. Il pourra s'agir de secrets partagés par les membres d'un
    cluster, ou un groupe d'hôtes collaborant autour d'une même
    application.

  - **Les secrets communs**  
    Ces secrets sont partagés par l'ensemble des hôtes d'un même
    inventaire.

Ces trois portées correspondent respectivement aux répertoires
``host_vars``, ``group_vars`` et ``groups_vars/all`` de tout inventaire
Ansible. Le magasin de secrets est structuré d'une manière identique,
chaque portée donnant lieu à un répertoire de premier niveau hébergeant
ses secrets.

#### Secrets d'hôtes
Tous les secrets d'hôtes sont stockés en-dessous du répertoire de
premier niveau ``hosts``. Chaque hôte Ansible se voit attribuer un
répertoire de second niveau pour le stockage de ses propres secrets.
Ce répertoire est nommé de manière identique au nom d'inventaire de
l'hôte Ansible propriétaire, ce qui revient à dire que chaque hôte
Ansible se voit attribuer un répertoire dédié à l'emplacement
``hosts/<inventory_hostname>``.

??? example "Exemple de répertoire dédié à un hôte Ansible"
    Par exemple, l'hôte ``ros.mgmt.ass-pve-1308-01`` pourra utiliser
    l'emplacement ``hosts/ros.mgmt.ass-pve-1308-01`` du magasin de
    pour y stocker ses secrets privés.

#### Secrets de groupes
Tous les secrets de groupes sont stockés en-dessous du répertoire de
premier niveau ``groups``. Chaque groupe Ansible se voit attribuer un
répertoire de second niveau pour le stockage de ses propres secrets.
Ce répertoire est nommé de manière identique au groupe d'inventaire
propriétaire, ce qui revient à dire que chaque groupe d'inventaire
se voit attribuer un répertoire dédié à l'emplacement
``groups/<group_name>``.

??? example "Exemple de répertoire dédié à un groupe d'inventaire Ansible"
    Le groupe d'inventaire ``app_instance__osticket__unique__db`` pourra
    utiliser l'emplacement ``groups/app_instance__osticket__unique__db``
    du magasin de pour y stocker ses secrets partagés.

#### Secrets communs
Tous les secrets communs sont stockés en-dessous du répertoire de
premier niveau ``all``.

### Organisation de l'arborescence
L'arborescence des magasins de secrets comprend trois répertoires de
premier niveau, correspondant aux portées de secrets définies plus haut:
Les secrets d'hôtes et de groupes sont ensuite répartis par noms d'hôtes
et de groupes, au moyen d'un répertoire de second niveau portant le nom
d'inventaire de l'hôte ou du groupe propriétaire du secret. Les secrets
communs ne comportent pas ce niveau intermédiaire. Ce système permet
de générer un répertoire racine pour chaque hôte ou groupe utilisant le
magasin, sous la forme:

  - ``hosts/<inventory_hostname>/`` pour les secrets d'hôtes
  - ``groups/<inventory_groupname>/`` pour les secrets de groupes
  - ``all/`` pour les secrets communs

#### Valeurs scalaires
A l'intérieur du répertoire racine, les secrets sont stockés à
l'intérieur d'une arborescence dont chaque niveau correspond à un
segment de la variable d'inventaire vouée à référencer le secret, les
segments ``my`` et ``our`` étant omis car superflus. Ceci signifie que
tout secret stocké dans le magasin doit être corrélé à une variable
d'inventaire susceptible de le représenter.

??? example "Exemple d'emplacement de secret pour une variable scalaire"
    Soit un hôte nommé ``ros.mgmt.ass-pve-1308-01``, et ayant l'usage
    d'une variable d'inventaire ``my_app_db_mysql_salt``, qui
    référence un secret. Ce secret pourra être stocké dans le magasin à
    l'emplacement ``hosts/ros.mgmt.ass-pve-1308-01/app/db/mysql/salt``.

#### Clés de types complexes
Si le secret correspond à la valeur d'une clé issue d'un type complexe,
le chemin d'accès intégrera, en plus des segments de la variable
d'inventaire corrélée, la valeur de la clé ``uid``, suivie du nom de
chaque clé parente de celle référençant le secret. Cette-dernière
constituera l'élément feuille.

??? example "Exemple d'emplacement de secret pour un type complexe"
    Soit un hôte nommé ``ros.mgmt.ass-pve-1308-01``, et ayant besoin
    de stocker le mot de passe de son utilisateur _root_. Dans
    l'inventaire Ansible, l'utilisateur _root_ est représenté par une
    instance du type de données [system.user][system.user], qui
    intègre une clé ``password``stockant son mot de passe, et une clé
    ``uid`` l'identifiant de manière unique parmi toutes les instances
    de [system.user][system.user] définies pour cet hôte. Cette instance
    décrivant _root_ est référencée par une variable partielle portant
    le nom ``my_system_users__root``, consolidée en ``my_system_users``.
    
    Le mot de passe de _root_ pourra donc être stocké à l'emplacement
    ``hosts/ros.mgmt.ass-pve-1308-01/system/users/root/password``
    du magasin de secrets.
