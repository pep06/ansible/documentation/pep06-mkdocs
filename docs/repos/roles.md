---
title: Rôles  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[bb-ansible]:       https://bitbucket.org/account/user/pep06/projects/ANSIBLE
[gl-ansible]:       https://gitlab.com/pep06/ansible
[gl-roles]:         https://gitlab.com/pep06/ansible/roles
[gl-roles-network]: https://gitlab.com/pep06/ansible/roles-network

[roles]:            /roles/
[role_provider]:    /roles/#metaroles-et-fournisseurs

# Dépôts de rôles

## Description

Les dépôts de rôles stockent les sources des rôles Ansible utilisés
par l'association. L'hébergement de ces dépôts est actuellement réparti
entre Bitbucket et GitLab, la migration du premier vers le second se
faisant progressivement.

Chez Bitbucket, les dépôts de rôles sont hébergés dans le projet
[``ANSIBLE``][bb-ansible].

Côté GitLab, le groupe [``ansible/roles``][gl-roles] sert de groupe
parent pour l'hébergement de l'ensemble des dépôts de rôles. Ceux-ci
sont répartis dans des sous-groupes correspondant à leur catégorie.

???+ example "Exemple"
    Un rôle nommé ``network/interfaces`` pourra être hébergé:

      - Dans le projet [``ANSIBLE``][bb-ansible] chez Bitbucket
      - Dans le groupe [``ansible/roles/network``][gl-roles-network]
        chez GitLab

## Nommage

La convention de nommage diffère selon que le dépôt est hébergé chez
Bitbucket ou GitLab.

Chez Bitbucket, le nom de chaque dépôt est formé sur le modèle
``ansible-role-<role_category>-<role_name>``. Chez GitLab, ce nom est
simplement ``<role_name>``.

Dans les deux cas:

- ``<role_category>`` est la catégorie à laquelle appartient le rôle

- ``<role_name>`` est le nom du rôle, sans mention de sa catégorie
  mais incluant un éventuel [suffixe de _fournisseur_][role_provider].

???+ example "Exemple"
    Un rôle nommé ``network/interfaces.ifupdown`` pourra être nommé:

      - ``ansible-role-network-interfaces.ifupdown`` chez Bitbucket
      - ``interfaces.ifupdown`` chez GitLab

## Liste

La liste des dépôts de rôle est maintenue dans une [section dédiée]
[roles].
