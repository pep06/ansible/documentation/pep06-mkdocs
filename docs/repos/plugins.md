---
title: Plugins tiers  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[gl-plugins]: https://gitlab.com/pep06/ansible/plugins
[gl-plugins-action-merge_vars]: https://gitlab.com/pep06/ansible/plugins/action-merge_vars

# Dépôts de plugins tiers

## Description

Les dépôts de plugins tiers stockent les sources Python des plugins
Ansible utilisés par l'association. Il peut s'agit de plugins développés
ou maintenus par nos soins, ou de plugins externes dont nous conservons
copie. Ces dépôts sont hébergés dans le groupe GitLab
[``ansible/plugins``][gl-plugins].

## Nommage

Les dépôts de plugins tiers sont nommés à partir du modèle
``<plugin_type>-<plugin_name>``, où:

- ``<plugin_type>`` est un type de plugin Ansible tel que ``action``,
  ``strategy``, ``filter``, ``lookup``...

- ``<plugin_name>`` est le nom du plugin.

## Liste
Nous ne maintenons actuellement qu'un seul dépôt de plugins tiers:

| Dépôt                                                 | Description                                              |
| ----------------------------------------------------- | -------------------------------------------------------- |
| [``action-merge_vars``][gl-plugins-action-merge_vars] | Fournit un mécanisme paramétrable de fusion de variables |
