---
title: Modules tiers  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[gl-modules]:         https://gitlab.com/pep06/ansible/modules
[gl-modules-freebsd]: https://gitlab.com/pep06/ansible/modules/freebsd

# Dépôts de modules tiers

## Description

Les dépôts de modules sont utilisés pour stocker les sources Python
de modules Ansible tiers. Ces dépôts sont hébergés dans le
groupe GitLab [``ansible/modules``][gl-modules].

### Nommage

Le nommage des dépôts de modules n'obéit à aucune convention. En règle
générale, chaque dépôt héberge un ou plusieurs modules liés à une
technologie ou une plateforme spécifique.

## Liste

Nous ne maintenons actuellement qu'un seul dépôt de modules tiers, qui
fournit des fonctionnalités propres aux plateformes FreeBSD:

| Dépôt                             | Description                                           |
| --------------------------------- | ----------------------------------------------------- |
| [``freebsd``][gl-modules-freebsd] | Fournit les modules ``kld`` et ``sysrc`` pour FreeBSD |
