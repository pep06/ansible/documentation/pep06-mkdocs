---
title: Documentation  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[gl-documentation]:              https://gitlab.com/pep06/ansible/documentation
[gl-documentation-pep06-mkdocs]: https://gitlab.com/pep06/ansible/documentation/pep06-mkdocs

# Dépôts de documentation

## Description

L'ensemble de la documentation concernant Ansible, dont les sources de
la présente, est consignée dans des dépôts hébergés dans le groupe
GitLab [``ansible/documentation``][gl-documentation].

## Nommage

Les dépots de documentation sont nommés selon le modèle
``<organization>-<format>``, où:

- ``<organization>`` est le nom de l'organisation couverte par la
  documentation.

- ``<format>`` est le format de la documentation (par exemple
  ``mkdocs``)

## Liste

Actuellement, ce projet ne comporte qu'un seul dépôt, qui contient les
sources de la présente documentation:

| Dépôt                                             | Description                                      |
| ------------------------------------------------- | ------------------------------------------------ |
| [``pep06-mkdocs``][gl-documentation-pep06-mkdocs] | Documentation Ansible des PEP06 au format mkdocs |
