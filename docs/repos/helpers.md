---
title: Utilitaires  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[gl-helpers]:             https://gitlab.com/pep06/ansible/helpers
[gl-helpers-generators]:  https://gitlab.com/pep06/ansible/helpers/generators

# Dépôts d'utilitaires

## Description

Les dépôts d'utilitaires stockent les sources de divers outils utilisés
en renfort du moteur Ansible, telles que des bibliothèques tierces ou
des modèles Jinja2 partagés par plusieurs rôles. Ces dépôts sont
hébergés dans le groupe GitLab [``ansible/helpers``][gl-helpers].

### Nommage

Le nommage des dépôts d'utilitaires n'obéit à aucune convention. En
règle générale, chaque dépôt est dédié à un outil spécifique dont il
prend le nom.

## Liste

Nous ne maintenons actuellement qu'un seul dépôt d'utilitaires, qui
stocke les modèles Jinja2 partagés par plusieurs de nos rôles:

| Dépôt                                   | Description                                                              |
|-----------------------------------------|--------------------------------------------------------------------------|
| [``generators``][gl-helpers-generators] | Modèles Jinja2 partagés utilisés pour la génération de données courantes |
