---
title: Inventaires  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[gl-inventories]:               https://gitlab.com/pep06/ansible/inventories
[gl-inventories-pep06-3.1511]:  https://gitlab.com/pep06/ansible/inventories/pep06-3.1511
[gl-inventories-pep06-4.1801]:  https://gitlab.com/pep06/ansible/inventories/pep06-4.1801
[gl-inventories-pep06-public]:  https://gitlab.com/pep06/ansible/inventories/pep06-public

# Dépôts d'inventaires

## Description

Les dépôts d'inventaires ont vocation à stocker les différents
inventaires utilisés avec Ansible. Ces dépôts sont hébergés dans le
groupe GitLab [``ansible/inventories``][gl-inventories].

## Nommage

Les dépôts d'inventaires sont nommés selon le modèle
``<organization>-<context>``, où:

- ``<organization>`` est le nom de l'organisation décrite par
  l'inventaire

- ``<context>`` est une chaîne arbitraire permettant d'indiquer le
  contexte auquel appartiennent les hôtes inventoriés. Ordinairement,
  il s'agit d'un nom de plan d'adressage ou d'hébergeur.

## Liste

Nous utilisons actuellement trois dépôts d'inventaires, en fonction
du contexte IP des hôtes référencés:

| Dépôt                                           | Description                                               |
| ----------------------------------------------- | --------------------------------------------------------- |
| [``pep06-3.1511``][gl-inventories-pep06-3.1511] | Hôtes employant la révision 3.1511 du plan d'adressage IP |
| [``pep06-4.1801``][gl-inventories-pep06-4.1801] | Hôtes employant la révision 4.1801 du plan d'adressage IP |
| [``pep06-public``][gl-inventories-pep06-public] | Hôtes adressés via l'Internet public                      |
