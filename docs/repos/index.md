---
title: Vue d'ensemble  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[git]:              https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git
[bb-ansible]:       https://bitbucket.org/account/user/pep06/projects/ANSIBLE
[gl-ansible]:       https://gitlab.com/pep06/ansible
[gl-documentation]: https://gitlab.com/pep06/ansible/documentation
[gl-helpers]:       https://gitlab.com/pep06/ansible/helpers
[gl-inventories]:   https://gitlab.com/pep06/ansible/inventories
[gl-modules]:       https://gitlab.com/pep06/ansible/modules
[gl-playbooks]:     https://gitlab.com/pep06/ansible/playbooks
[gl-plugins]:       https://gitlab.com/pep06/ansible/plugins
[gl-roles]:         https://gitlab.com/pep06/ansible/roles
[gl-workspaces]:    https://gitlab.com/pep06/ansible/workspaces
[keybase]:          https://keybase.io/

# Vue d'ensemble des dépôts Ansible

## Hébergeurs

Toutes les données liées à Ansible sont soumises au contrôle de version,
et stockées dans des dépôts [Git][git] répliqués chez un hébergeur
externe. Actuellement, ces répliques sont réparties sur les trois
hébergeurs suivants:

- **Bitbucket**  
  [Bitbucket][bb-ansible] est notre hébergeur _Git_ historique, que
  nous sommes en train de quitter au profit de GitLab. Tous les
  dépôts hébergés chez Bitbucket sont privés. La création de nouveaux
  dépôts est interdite. Seul le maintien des dépôts existants est
  autorisé.

- **GitLab**  
  [GitLab][gl-ansible] est notre nouvel hébergeur de référence.
  Nos dépôts y sont progressivement migrés depuis Bitbucket. Les
  dépôts de rôles, plugins et modules y sont désormais publics.

- **Keybase**  
  [Keybase] fournit un hébergement de dépôts Git chiffrés. Nous
  y avons recours pour héberger ceux de nos dépôts qui contiennent
  les données les plus sensibles. L'accès y est extrêmement restreint.

## Catégories de dépôts

Le tableau ci-dessous présente l'ensemble des catégories de dépôts
actuellement définies:

| Identifiant       | Contenu            | Hébergeur(s)                                | Accès  |
| ----------------- | ------------------ | ------------------------------------------- | ------ |
| ``documentation`` | Documentation      | [GitLab][gl-documentation]                  | Public |
| ``helpers``       | Utilitaires        | [GitLab][gl-helpers]                        | Public |
| ``inventories``   | Inventaires        | [GitLab][gl-inventories]                    | Privé  |
| ``modules``       | Modules tiers      | [GitLab][gl-modules]                        | Public |
| ``playbooks``     | Playbooks          | [GitLab][gl-playbooks]                      | Privé  |
| ``plugins``       | Plugins tiers      | [GitLab][gl-plugins]                        | Public |
| ``roles``         | Rôles              | [Bitbucket][bb-ansible], [GitLab][gl-roles] | Public |
| ``secrets``       | Secrets            | [Keybase][keybase]                          | Privé  |
| ``workspaces``    | Espaces de travail | [GitLab][gl-workspaces]                     | Privé  |

Chaque catégorie de rôle fait l'objet d'une sous-page dédiée en
décrivant les principaux aspects.
