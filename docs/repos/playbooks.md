---
title: Playbooks  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[gl-playbooks]:       https://gitlab.com/pep06/ansible/playbooks
[gl-playbooks-pep06]: https://gitlab.com/pep06/ansible/playbooks/pep06

# Dépôts de playbooks

## Description

Les playbooks que nous utilisons avec Ansible sont stockés dans des
dépôts dédiés afin de pouvoir être partagés par plusieurs espaces de
travail. Ces dépôts sont hébergés dans le groupe GitLab
[``ansible/playbooks``][gl-playbooks].

## Nommage

Pour le moment, chaque organisation ne peut détenir qu'un seul dépôt
de playbooks, dont le nom doit correspondre à celui de l'organisation
propriétaire.

## Liste

Nous ne maintenons actuellement qu'un seul dépôt de playbooks pour le
compte des PEP06:

| Dépôt                           | Description                      |
| ------------------------------- | -------------------------------- |
| [``pep06``][gl-playbooks-pep06] | Playbooks utilisés par les PEP06 |
