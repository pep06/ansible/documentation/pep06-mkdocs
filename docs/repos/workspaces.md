---
title: Espaces de travail  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[gl-workspaces]:        https://gitlab.com/pep06/ansible/workspaces
[gl-workspaces-pep06]:  https://gitlab.com/pep06/ansible/workspaces/pep06

[repos-documentation]: ../documentation/
[repos-inventories]:   ../inventories/
[repos-modules]:       ../modules/
[repos-playbooks]:     ../playbooks/
[repos-plugins]:       ../plugins/
[repos-roles]:         ../roles/
[repos-secrets]:       ../secrets/

# Dépôts d'espaces de travail

## Description

Les dépôts d'espaces de travail réunissent l'ensemble des ressources
formant un espace de travail Ansible. Ces ressources incluent divers
fichiers de configuration, ainsi que les références à l'ensemble des
autres dépôts requis par l'environnement Ansible. Les dépôts d'espaces
de travail constituent les dépôts parents à partir desquels tous les
autres dépôts sont référencés sous la forme de sous-modules.

Les dépôts d'espaces de travail sont hébergés dans le groupe GitLab
[``ansible/workspaces``][gl-workspaces].

## Nommage

Le nommage des dépôts d'espaces de travail est libre. Ordinairement,
le nom correspond à celui d'une organisation.

## Liste

Nous ne maintenons actuellement qu'un seul dépôt d'espace de travail,
qui correspond à l'espace de travail Ansible principal des PEP06:

| Dépôt                            | Description                           |
| -------------------------------- | ------------------------------------- |
| [``pep06``][gl-workspaces-pep06] | Espace de travail principal des PEP06 |

## Organisation

Les dépôts d'espaces de travail sont organisés selon le modèle suivant:

```
/
|-- backups/
|-- docs/
|-- inventories/
|-- org∕
|-- playbooks/
|-- plugins/
|-- roles/
|-- secrets/
|-- tasks/
```

Le tableau suivant décrit le rôle de chaque sous-répertoire. Certains
de ces répertoires, tels que ceux contenant des fichiers de sauvegardes
non-chiffrés, sont exclus des dépôts de playbooks via un fichier
``.gitignore``.

| Répertoire       | Contenu                                  | Type                                                |
| ---------------- | ---------------------------------------- | --------------------------------------------------- |
| ``backups/``     | Fichiers de sauvegardes                  | Fichiers sensibles exclus du contrôle de versions   |
| ``docs/``        | Sources de la documentation              | Sous-module de [documentation][repos-documentation] |
| ``inventories/`` | Inventaires Ansible                      | Sous-modules d'[inventaires][repos-inventories]     |
| ``library/``     | Modules tiers                            | Sous-modules de [modules tiers][repos-modules]      |
| ``org/``         | Données d'organisations                  | Sous-modules de données d'organisations             |
| ``playbooks/``   | Playbooks Ansible                        | Sous-modules de [playbooks][repos-playbooks]        |
| ``plugins/``     | Plugins tiers                            | Sous-modules de [plugins tiers][repos-plugins]      |
| ``roles/``       | Rôles organisés par catégorie            | Sous-modules de [rôles][repos-roles]                |
| ``secrets/``     | Données secrètes (clés et mots de passe) | Sous-modules de [secrets][repos-secrets]            |
| ``tasks/``       | Fichiers de tâches réutilisables         | Fichiers YAML                                       | 
