---
title: Secrets  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[keybase]: https://keybase.io
[kb-secrets-pep06]: keybase://team/adpep06.it.sysops.ansible/secrets-pep06

# Dépôts de secrets

## Description

Les dépôts de secrets ont vocation à stocker des informations sensibles
devant demeurer secrètes, telles que des mots de passe ou des clés
de chiffrement.

Les données y sont stockées sous une forme chiffrée, et ne seraient donc
pas exploitables par un tiers obtenant copie du dépôt. Néanmoins, pour
plus de sécurité, ces dépôts ne sont pas hébergés par nos prestataires
habituels mais chez [Keybase][keybase], qui propose un service
d'hébergement Git chiffré hautement sécurisé. Ces dépôts sont hébergés
dans l'équipe Keybase ``adpep06.it.sysops.ansible``.

## Nommage

Les dépôts de secrets sont liés à une organisation, et optionnellement
à un inventaire. Par conséquent, leur nom est construit à partir du
modèle ``secrets-<organization>[-<context>]``, où:

- ``<organization>`` est le nom de l'organisation à laquelle
  appartiennent les informations secrètes.

- ``<context>`` est une chaîne arbitraire correspondant au contexte
  d'un inventaire spécifique. Lorsque le dépôt de secrets est lié à un
  inventaire spécifique, ``<context>`` prend la même valeur que dans
  le nom du dépôt d'inventaire auquel il se rapporte.

## Liste

Nous maintenons actuellement les dépôts de secrets suivants:

| Dépôt                                 | Description                |
| ------------------------------------- | -------------------------- |
| [``secrets-pep06``][kb-secrets-pep06] | Données secrètes des PEP06 |