---
title: Types de données  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[pykwalify]: https://pykwalify.readthedocs.io/en/latest/
[variables]: #
[xmllint]: http://xmlsoft.org/xmllint.html

# Types de données

Cette section contient les descriptions et schémas autoritaires des
types de données PEP06 manipulés par Ansible. Ces schémas permettent
de valider les variables définies à l'intérieur d'Ansible contre une
norme faisant autorité.

## Introduction
Pour effectuer ses tâches de configuration, Ansible s'appuie sur
diverses données définies au moyen de [variables][variables]. Afin
de faciliter la gestion des environnements comprenant plusieurs
centaines de variables, nous avons fait le choix de définir des **types
de données**, afin de typer les variables.

Chaque type de données définit la structure attendue des variables se
réclamant de son type, ainsi, idéalement, qu'un schéma permettant leur
validation.

Cette section de la documentation a pour ambition de lister la totalité
des types de données connus avec leurs caractéristiques.

## Caractéristiques des types de données
### Nommage
#### Identifiant unique
Chaque type de données hébergé dans cette bibliothèque possède un
identifiant unique (_UID_), qui se présente sous la forme d'une chaîne
de caractères, divisée en plusieurs segments séparés par le signe point
(``.``). Ces segments permettent de définir une hiérarchie d'espaces
de noms, à la manière de ceux utilisés par de nombreux langages de
programmation pour encapsuler leurs classes.

Afin de simplifier la navigation parmi les types, il est d'usage de
construire leur UID en chaînant les segments par poids décroissant,
c'est-à-dire du plus général au particulier.

???+ example "Illustration de la construction d'UIDs par poids décroissant"
    Un type de données représentant une règle de pare-feu pourra
    par exemple porter l'UID ``sys.net.filter.rule``. Dans cet UID:

      - ``sys`` désigne la notion générale de _système_, c'est-à-dire de
        _système d'exploitation_. Ce segment situe d'emblée ce type de
        données dans un contexte système ou noyau.
      - ``net`` est l'abbréviation de _network_. Parce qu'il vient après
        ``sys``, il restreint encore davantage le contexte du type de
        données au sous-système réseau d'un système d'exploitation.
      - ``filter`` désigne un filtre de paquets, qui est le nom savant
        d'un pare-feu.
      - ``rule`` désigne une règle de filtre de paquet, c'est-à-dire un
        sous-élément de ``filter``. Ce segment apparaît donc en
        dernière position.

    Cette technique de nommage garantit par exemple que tous les types
    se rapportant au réseau apparaîtront groupés à l'intérieur d'une
    liste de types classés par ordre alphabétique, puisque leurs UIDs
    débuteront tous par ``sys.net.``.

#### Hiérarchie des types
Par ce système de segments, il devient possible d'organiser les types
de données sous la forme d'une _hiérarchie d'espaces de noms_. Le
sous-conteneur _Bibliothèque_ de cet onglet vous permet de naviguer à
l'intérieur de cette hiérarchie.

#### Préfixe organisationnel
Afin d'éviter les collisions de noms avec les types émanant d'entités
tierces, les identifiants des types de données des PEP06 sont préfixés
par le _préfixe organisationnel_ ``fr.pep06.schemas``.

???+ example "Exemple d'usage du préfixe organisationnel"
    Reprenons l'exemple de notre règle de pare-feu: l'UID
    ``sys.net.filter.rule``, une fois préfixé, devient
    ``fr.pep06.schemas.sys.net.filter.rule``.

Chaque segment du préfixe organisationnel décrit un niveau d'une
hiérarchie d'espaces de noms imbriqués les uns dans les autres.
Les segments de tête doivent obligatoirement correspondre au suffixe DNS
de l'organisation propriétaire du schéma, pris en sens inverse.

Dans le cas des PEP06, cela implique que **tous les noms de schémas
doivent commencer par ``fr.pep06.``**. Si nous devions héberger des
schémas maintenus par les PEP83, leurs identifiants uniques devraient
inclure le préfixe organisationnel ``org.pep83.``

En outre, les PEP06 s'infligent une contrainte supplémentaire en
forçant l'usage de l'espace de noms ``fr.pep06.schemas.``.

???+ note "Omission du préfixe organisationnel"
    La majorité des schémas présents dans cette documentation
    appartenant aux PEP06, le préfixe organisationnel de l'association
    est souvent omis pour alléger les UIDs des types de données.
    Par conséquent, en l'absence de préfixe organisationnel, le préfixe
    ``fr.pep06.schemas`` est implicitement assumé.

### Schémas
Chaque type de données doit idéalement fournir un schéma de validation
permettant de valider ses instances. Cet idéal n'est pas toujours
atteignable car tous ne supportent pas la validation (CSV, par exemple,
ne supporte pas la validation par schéma).

Certains langages de balisage, tels que XML, peuvent être validés au
moyen de plusieurs types de schémas (DTD, XSD, RelaxNG...). Le tableau
ci-dessous énumère la liste des types de schémas autorisés dans la
présente documentation:

| Langage de balisage | Type de schéma | Extension | Statut   | Valideur recommandé    |
| ------------------- | -------------- | --------- | -------- | ---------------------- |
| XML                 | XSD            | ``.xsd``  | Autorisé | [xmllint][xmllint]     |
| YAML                | kwalify        | ``.yml``  | Autorisé | [pykwalify][pykwalify] |

Les schémas XSD peuvent être validés à l'aide d'un parser XML tel que
_xmllint_.

## Organisation de la bibliothèque de types
La sous-section _Bibliothèque_ référence la totalité des types de
données utilisables. La biliothèque est organisée par préfixe
organisationnel, puis par identifiant unique de type.
