---
title: pki.ssh.keypair  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# Type de données ``fr.pep06.schemas.pki.ssh.keypair``

Ce type de données décrit une **paire de clés de chiffrement SSH**.

## Description
Ce type de données permet de définir une paire de clés de chiffrement
asymétrique SSH, qui peut ensuite être affectée à un hôte ou un
uitlisateur.

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.pki.ssh.keypair
uid: <str>
tags: <str[]?>
algo: <str(rsa|dsa|ecdsa|ed25519)>
usage: <str(host|service|user)>
principal: <str>
public:
  openssh:  <str>
private:
  openssh: <str?>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``uid`` (**str**, _obligatoire_)
Une chaîne servant d'identifiant unique de la paire de clé.

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées à la paire de clés. Ces
étiquettes permettent au besoin de filtrer la liste des clés. Les
étiquettes couramment utilisées avec les paires de clés sont listées
dans le tableau ci-dessous:

| Valeur      | Description                                              |
|-------------|----------------------------------------------------------|
| ``revoked`` | Indique que la paire de clés a été révoquée              |

Les paires de clés révoquées sont conservées dans l'inventaire afin de
pouvoir vérifier qu'elles sont absentes des hôtes gérés par Ansible.

#### ``algo`` (**str**, _obligatoire_)
Le nom de l'algorithme utilisé pour générer la paire de clés. Cette clé
accepte les valeurs suivantes:

| Valeur      | Description        |
| ----------- | ------------------ |
| ``rsa``     | Algorithme RSA     |
| ``dsa``     | Algorithme DSA     |
| ``ecdsa``   | Algorithme ECDSA   |
| ``ed25519`` | Algorithme ED25519 | 

#### ``usage`` (**str**, _obligatoire_)
La clé ``usage`` indique l'usage auquel la paire de clés est destinée.
Cette clé peut prendre une des valeurs ci-dessous:

| Valeur      | Description                                             |
|-------------|---------------------------------------------------------|
| ``host``    | Indique que la paire de clé authentifie un hôte         |
| ``service`` | Indique que la paire de clé authentifie un individu     |
| ``user``    | Indique que la paire de clé authentifie un service      |

#### ``principal`` (**str**, _obligatoire_)
Le principal associé à la paire de clés. Le format du principal dépend
de la valeur de la clé ``usage``. Le tableau ci-dessous résume les
formats de principaux disponibles:

| Type de paire de clés | Format de principal       |
|-----------------------|---------------------------|
| ``host``              | ``<hostname>``            |
| ``user``              | ``<username>@<hostname>`` |

#### ``public.openssh`` (**str**, _obligatoire_)
Une clé publique au format OpenSSH, sous la forme d'une chaîne de
caractères sans retour à la ligne.

#### ``private.openssh`` (**str**, _optionnel_, _chiffrée_)
Une clé privée, au format PEM OpenSSH, chiffrée via Ansible Vault ou
PGP. Bien que ce type de données prévoie cette fonctionnalité, le
stockage de clés privées dans les variables d'inventaire est prohibé.
