---
title: storage.zfs.pool  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[storage.zfs.vdev]: ../storage.zfs.vdev/

# Type de données ``fr.pep06.schemas.storage.zfs.pool``

Ce type de données décrit un **pool de stockage ZFS**.

## Description
Ce type de données permet de représenter un pool de stockage ZFS.

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.storage.zfs.pool
uid: <str>
name: <str>
description: <str?>
managed: <bool?>
tags: <str[]?>
properties: <dict?>
# !fr.pep06.schemas.storage.zfs.vdev[]
vdevs: <storage.zfs.vdev[]>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``uid`` (**str**, _obligatoire_)
Cette clé permet de définir un identifiant unique associé au pool de
stockage ZFS. Cet identifiant sera uniquement utilisé à l'intérieur
d'Ansible, et n'apparaîtra à aucun moment sur le système cible.

#### ``name`` (**str**, _obligatoire_)
Le nom du pool de stockage ZFS. Cette clé accepte une valeur chaîne
uniquement constituée de caractères alphanumériques, underscore (``_``)
et tiret (``-``).

#### ``description`` (**str**, _optionnel_)
Cette clé permet d'associer une courte description au pool de stockage
ZFS.

#### ``managed`` (**bool**, _optionnel_)
Ce drapeau indique si le pool de stockage ZFS doit ou non être géré par
Ansible. Il peut prendre les valeurs suivantes:

| Valeur                | Description                                                       |
| --------------------- | ----------------------------------------------------------------- |
| **``true`` (défaut)** | Le pool de stockage ZFS est géré par Ansible                      |
| ``false``             | Le pool de stockage ZFS n'est pas géré par Ansible et sera ignoré |

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées au pool de stockage. Ces
étiquettes permettent aux rôles de filtrer la liste des pools ZFS
afin de cibler uniquement ceux qui les intéressent. Le tableau
ci-dessous liste les étiquettes couramment utilisées:

| Valeur     | Description                                                     |
| ---------- | --------------------------------------------------------------- |
| ``boot``   | Indique que le pool ZFS est utilisé pour l'amorçage du système  |

#### ``properties`` (**dict**, _optionnel_)
Une liste optionnelle de propriétés associées au pool de stockage ZFS.
Cette clé référence un dictionnaire, mappant un nom de propriété
(``str``) à la valeur de cette propriété (``str``). La liste complète
des propriétés supportées par les pools ZFS varie selon la version
utilisée, et est consultable via le manuel.

#### ``vdevs`` (**storage.zfs.vdev[]**, _obligatoire_)
La liste des _vdevs_ constituant le pool ZFS. Chaque pool ZFS doit
comprendre au moins un vdev, représenté par une entrée de cette liste.
Chaque entrée de liste est un ``dict`` constituant une instance du type
[``storage.zfs.vdev``][storage.zfs.vdev]