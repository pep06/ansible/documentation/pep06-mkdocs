---
title: network.gateway  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[my_network_interfaces]: /inventory/variables/library/my/my_network_interfaces/
[network.interface]: ../network.interface/

# Type de données ``fr.pep06.schemas.network.gateway``

Ce type de données décrit une **passerelle** au sein d'un réseau
IP.

## Description
Ce type de données permet de représenter une passerelle participant à
un réseau IP. Chaque passerelle peut-être adressée au moyen d'IPv4,
d'IPv6 ou des deux à la fois si elle est en double pile.

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.network.gateway
uid: <str>
tags: <str[]?>
description: <str?>
interface: <str>
addresses:
  inet4: <str?>
  inet6: <str?>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``uid`` (**str**, _obligatoire_)
Un identifiant unique associé à la passerelle. Cet identifiant pourra
être utilisé pour référencer la passerelle à partir d'autres contextes.
Cet identifiant doit être court, et uniquement composé de caractères
alphanumériques, underscore et tirets (``/^[a-zA-Z0-9_-]+$/``).

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées à la passerelle. Ces
étiquettes permettent aux rôles de filtrer la liste des passerelles
afin de cibler uniquement celles qui les intéressent. Le tableau
ci-dessous liste quelques étiquettes couramment affectées aux
passerelles:

| Etiquette   | Description                                |
| ----------- | ------------------------------------------ |
| ``default`` | Indique une passerelle utilisée par défaut |

#### ``description`` (**str**, _optionnel_)
Une courte description de la passerelle.

#### ``interface`` (**str**, _obligatoire__)
Cette clé indique l'interface réseau par laquelle la passerelle est
joignable. L'[interface cible][network.interface] est désignée par son
identifiant unique, qui correspond à la valeur de sa clé ``uid``.

Lorsque l'identifiant unique de l'interface est variable, ou ne peut pas
être connu à l'avance, la valeur de cette clé peut être affectée
dynamiquement via un jeu de filtres Jinja appliqués à la variable
[``my_network_interfaces``][my_network_interfaces].

#### ``addresses.inet4`` (**str**, _optionnel_)
L'adresse IPv4 de la passerelle, si elle supporte ce protocole.

#### ``addresses.inet6`` (**str**, _optionnel_)
L'adresse IPv6 de la passerelle, si elle supporte ce protocole.
