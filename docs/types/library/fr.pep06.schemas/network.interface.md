---
title: network.interface  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[^1]: https://www.kernel.org/doc/Documentation/networking/bonding.txt
[^2]: https://www.freebsd.org/cgi/man.cgi?query=ifconfig&sektion=8

# Type de données ``fr.pep06.schemas.network.interface``

Ce type de données décrit une **interface réseau** connectée à un hôte
Ansible.

## Description
Ce type de données permet de représenter une interface réseau. Il peut
s'agir une interface physique (adaptateur) ou virtuelle (bridge,
  vlan...).

## Structure attendue
Ce type de données doit satisfaire à la structure ci-dessous. Le type
étant incomplet, les instances actuellement présentes dans les
inventaires comportent très probablement des clés supplémentaires qui
ne sont pas documentées ici.

````yaml
---
# !fr.pep06.schemas.network.interface
# Clés communes
uid: <str>
name: <str?>
description: <str?>
tags: <str[]?>
zone: <str?>
type: <str(adapter|bond|bridge|vlan)>
autostart:
  on_boot: <bool?>
  on_plug: <bool?>

# Interfaces de type 'adapter'
adapter:
  device: <str>

# Interfaces de type 'bond'
bond:
  index: <int>
  members: <str[]>
  mode: <str(broadcast|failover|hash|lacp|roundrobin)>
  hash_sources: <int[]?>

# Interfaces de type 'bridge'
bridge:
  index: <int>
  members: <str[]>
  stp:
    enabled: <bool?>
    delay: <int(4..30)?>
    hello_time: <int(1..2)?>
    priority: <int(0..61440)?>
    variant: <str(classic|rapid)?>
  vlan:
    enabled: <bool?>
    range: <str?>

# Interfaces de type 'vlan'
vlan:
  index: <int>
  parent: <str[]>

# Configuration Ethernet
ethernet:
  address: <str?>
  mtu: <int?>

# Configuration IPv4
inet4:
  enabled: <bool?>
  dhcp:
    enabled: <bool?>
  static:
    enabled: <bool?>

# Configuration IPv6
inet6:
  enabled: <bool?>
  autoconf:
    enabled: <bool?>
  static:
    enabled: <bool?>
````

### Clés communes
Les clés documentées dans cette section sont communes à l'ensemble des
instances de ce type.

#### ``uid`` (**str**, _obligatoire_)
Un identifiant unique associé à l'interface. Cet identifiant pourra
être utilisé pour référencer l'interface à partir d'autres contextes.
Cet identifiant doit être court, et uniquement composé de caractères
alphanumériques, underscore et tirets (``/^[a-zA-Z0-9_-]+$/``).

#### ``name`` (**str**, _optionnel_)
Un nom personnalisé affecté à l'interface réseau.

Si cette clé est définie, les rôles chargés de générer la configuration
réseau tenteront, dans la mesure des contraintes techniques, de baptiser
l'interface conformément à la valeur définie ici.

Si cette clé est absente, une interface physique conservera son nom
d'origine ou bien, et une interface virtuelle sera nommée d'après les
conventions en vigueur sur la plateforme de destination.

Les noms d'interfaces doivent respecter les mêmes contraintes que les
identifiants uniques, à savoir être courts, et uniquement composés de
caractères alphanumériques, underscore et tirets
(``/^[a-zA-Z0-9_-]+$/``).

#### ``description`` (**str**, _optionnel_)
Une courte description de l'interface réseau.

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées à l'interface. Ces
étiquettes permettent aux rôles de filtrer la liste des interfaces
afin de cibler uniquement celles qui les intéressent.

#### ``zone`` (**str**, _optionnel_)
Un identifiant de zone optionnellement associé à l'interface réseau.
L'identifiant de zone correspond à la classe du sous-réseau auquel
l'interface est connectée. Cette clé doit être omise si l'interface
n'est pas adressée L3.

#### ``type`` (**str**, _obligatoire_)
Le type de l'interface réseau. Cette clé accepte les valeurs suivantes:

| Valeur      | Description                  |
| ----------- | ---------------------------- |
| ``adapter`` | Interface de type adaptateur |
| ``bond``    | Interface de type équipe     |
| ``bridge``  | Interface de type pont       |
| ``vlan``    | Interface de type vLAN       |


#### ``autostart.on_boot`` (**bool**, _optionnel_)
Cette clé indique si l'interface réseau doit être automatiquement
configurée au démarrage du système hôte.

#### ``autostart.on_plug`` (**bool**, _optionnel_)
Cette clé indique si l'interface réseau doit être automatiquement
configurée si le périphérique qu'elle représente est connecté à chaud
au système hôte.

### Interfaces de type ``adapter``
Les clés documentées dans cette section sont uniquement applicables aux
interfaces de type adaptateur, c'est-à-dire dont la clé
[``type``](#type-str-obligatoire) vaut ``adapter``.

#### ``adapter.device`` (**str**, _obligatoire_)
Cette clé définit le nom du périphérique réseau représenté par
l'interface.

### Interfaces de type ``bond``
Les clés documentées dans cette section sont uniquement applicables aux
interfaces de type équipe, c'est-à-dire dont la clé
[``type``](#type-str-obligatoire) vaut ``bond``.

#### ``bond.index`` (**int**, _obligatoire_)
Cette clé assigne le numéro d'index statique qui sera utilisé pour
nommer le périphérique représentant l'interface d'équipe. Cette valeur
numérique sera accolée au nom du pilote en charge des interfaces
d'équipes sur le système hôte, afin de former le nom final du
périphérique (par exemple ``bond<index>`` sur les systèmes GNU/Linux,
ou ``lagg<index>`` en environnement FreeBSD).

Cette clé est obligatoire afin de garantir l'unicité du nom du
périphérique lié à l'interface, même lorsque la clé ``name`` est
définie, le renommage des interfaces intervenant *après* la création du
périphérique initial.

#### ``bond.members`` (**str[]**, _obligatoire_)
Cette clé référence la liste des interfaces réseau assignées à l'équipe.
Ces interfaces sont désignées par la valeur de leur propriété ``uid``.
Toute interface d'équipe doit posséder au moins un membre pour être
considérée comme valide.

#### ``bond.mode`` (**str**, _obligatoire_)
Cette clé indique le mode de répartition des flux au sein de l'équipe.
Sa présence est obligatoire, et elle peut prendre l'une des valeurs
suivantes:

| Valeur         | Description                                         |
|----------------|-----------------------------------------------------|
| ``broadcast``  | Duplication du traffic sur toutes les interfaces    |
| ``failover``   | Un seul lien actif à la fois                        |
| ``hash``       | Répartition basée sur une fonction de hachage       |
| ``lacp``       | Aggrégation de liens 802.3ad                        |
| ``roundrobin`` | Répartition séquentielle, interface après interface |

Les spécificités d'implémentation de chaque mode sur chaque plateforme
sont documentées au niveau rôle chargé de la configuration des
interfaces réseau.

#### ``bond.hash_sources`` (**str[]**, _optionnel_)
Cette clé définit les sources d'informations qu'un algorithme de
répartition basé sur le hachage doit utiliser pour générer les hashes
sur la base desquels le traffic sera réparti. Elle est seulement
pertinente lorque la clé ``bond.mode`` vaut ``hash`` ou ``lacp``.

Cette clé référence une liste de chaînes de caractères. Elle peut
comprendre une ou plusieurs des valeurs suivantes:

| Valeur | Description                                                   |
|--------|---------------------------------------------------------------|
| ``l2`` | Utiliser des informations issues de la couche 2 du modèle OSI |
| ``l3`` | Utiliser des informations issues de la couche 3 du modèle OSI |
| ``l4`` | Utiliser des informations issues de la couche 4 du modèle OSI |

???+ warning "Variations entre implémentations"
    Il est à noter que la nature des informations utilisées pour chaque
    couche OSI varie en fonction des plateformes et des implémentations.
    Ainsi, si ``l2`` est spécifié en environnement GNU/Linux, les hashes
    seront générés à partir des adresses MAC source et destination, et
    des identifiants de paquets[^1].
    En revanche, le même paramétrage sous FreeBSD entraînera la prise
    en compte des adresses MAC et des identifiants de vLANs[^2].

    De la même manière, toutes les plateformes ne supportent pas toutes
    les combinaisons de couches. GNU/Linux sera limité aux seules
    combinaisons ``['l2']``, ``['l2', 'l3']`` et ``['l2', 'l4']``[^1],
    alors que FreeBSD supportera n'importe quelle configuration[^2]. Les
    rôles chargés de la configuration réseau prennent en compte ces
    spécificités, et se chargeront de rendre au mieux les combinaisons
    incompatibles.

### Interfaces de type ``bridge``
Les clés documentées dans cette section sont uniquement applicables aux
interfaces de type pont, c'est-à-dire dont la clé
[``type``](#type-str-obligatoire) vaut ``bridge``.

#### ``bridge.index`` (**int**, _obligatoire_)
Cette clé assigne le numéro d'index statique qui sera utilisé pour
nommer le périphérique représentant l'interface de pont. Cette valeur
numérique sera accolée au nom du pilote en charge des interfaces de
ponts sur le système hôte, afin de former le nom final du périphérique
(par exemple ``br<index>`` sur les systèmes GNU/Linux, ou
``bridge<index>`` en environnement FreeBSD).

Cette clé est obligatoire afin de garantir l'unicité du nom du
périphérique lié à l'interface, même lorsque la clé ``name`` est
définie, le renommage des interfaces intervenant *après* la création du
périphérique initial.

#### ``bridge.members`` (**str[]**, _obligatoire_)
Cette clé référence la liste des interfaces réseau assignées au pont.
Ces interfaces sont désignées par la valeur de leur propriété ``uid``.
La présence de cette clé et obligatoire. Si l'interface de type pont
ne possède aucune interface membre, la clé doit explicitement référencer
une liste vide.

#### ``bridge.stp.enabled`` (**bool**, _optionnel_)
Cette clé indique si le protocole STP doit être activé sur l'interface
pont. L'état d'activation par défaut dépend de la plateforme cible.

#### ``bridge.stp.delay`` (**int**, _optionnel_)
Cette clé permet de définir une durée personnalisée pour la phase
d'apprentissage STP, pendant laquelle l'interface ne relaiera aucun
paquet. Cette clé requiert une valeur entière comprise entre 4 et 30.
L'unité est la seconde. La valeur par défaut varie selon les plateformes
mais se situe généralement autour de 15 secondes pour la variante STP.

#### ``bridge.stp.hello_time`` (**int**, _optionnel_)
Cette clé permet de définir un délai personnalisé entre deux émissions
de messages de configuration STP. Cette clé requiert une valeur entière
comprise entre 1 et 2. L'unité est la seconde. La valeur par défaut
varie selon les plateformes.

??? note "Support limité"
    Cette option n'est pas supportée par toutes les plateformes.

#### ``bridge.stp.priority`` (**int**, _optionnel_)
Cette clé permet de définir une priorité STP personnalisée pour
l'interface de pont. La valeur par défaut (32768) peut ainsi être
modifiée pour diminuer ou augmenter la priorité du noeud au sein de
l'arbre STP. Cette clé accepte une valeur entière comprise entre 0 et
61440. Cette valeur n'a pas d'unité. Une valeur plus faible indique une
priorité plus haute.

??? note "Support limité"
    Cette option n'est pas supportée par toutes les plateformes.

#### ``bridge.stp.variant`` (**str**, _optionnel_)
Cette clé permet de définir la variante du protocole STP à utiliser.
Elle accepte une des valeurs suivantes:

| Valeur      | Description                                      |
| ----------- | ------------------------------------------------ |
| ``classic`` | Utiliser la variante historique du protocole STP |
| ``rapid``   | Utiliser la variante rapide du protocole STP     |

??? note "Support limité"
    Cette option n'est pas supportée par toutes les plateformes.
    Les interfaces pont GNU/Linux ne supportent par exemple que la
    variante classique.

#### ``bridge.vlan.enabled`` (**bool**, _optionnel_)
Cette clé indique si l'interface pont doit ou non être configurée pour
autoriser la commutation de trames possédant des étiquettes de vLANs.
Cette option est uniquement pertinente sous Linux, où elle permet
d'activer/désactiver la gestion des vLANs sur une interface pont.

#### ``bridge.vlan.range`` (**str**, _optionnel_)
Cette clé permet de spécifier l'étendue des vLANs étiquetés que le pont
est autorisé à relayer. Elle n'est pertinente que sous Linux. Si cette
clé est omise, ou que l'interface pont est configurée sur une plateforme
non GNU/Linux, tous l'étendue maximale ``2-4094`` sera utilisée.

L'étendue doit être spécifiée selon le format ``<int>-<int>``, où le
premier entier indique le début de l'étendue, et le second sa fin. Ces
deux valeurs seront comprises dans l'étendue autorisée.

### Interfaces de type ``vlan``
Les clés documentées dans cette section sont uniquement applicables aux
interfaces de type vLAN, c'est-à-dire dont la clé
[``type``](#type-str-obligatoire) vaut ``vlan``.

#### ``vlan.index`` (**int**, _obligatoire_)
Cette clé permet de spécifier l'identifiant de vLAN (vLAN ID) du vLAN
à configurer. Elle est obligatoire, et sera aussi utilisée pour nommer
le périphérique virtuel représentant l'interface (typiquement nommé
d'après le patron ``vlan<index>``).

#### ``vlan.parent`` (**str**, _obligatoire_)
Cette clé permet de spécifier l'interface parente à laquelle connecter
le vLAN. Il s'agit d'un identifiant unique d'interface, correspondant
à la valeur de la clé ``uid`` de l'interface cible.

### Configuration Ethernet
Cette section documente les clés relatives à la configuration du
protocole Ethernet.

#### ``ethernet.address`` (**str**, _optionnel_)
Cette clé permet de définir une adresse matérielle (MAC) personnalisée
pour cette interface. Si elle est omise, l'adresse matérielle par défaut
sera utilisée.

#### ``ethernet.mtu`` (**int**, _optionnel_)
Cette clé permet de définir une valeur d'unité maximale de transmission
(MTU) personnalisée. Si elle est omise, la valeur par défaut est
assumée.

### Configuration IPv4
Cette section documente les clés relatives à la configuration d'IPv4.

#### ``inet4.enabled`` (**bool**, _optionnel_)
Cette clé indique si la suite de protocoles IPv4 doit être activée
sur l'interface. Si cette clé est omise, IPv4 sera désactivé.

#### ``inet4.dhcp.enabled`` (**bool**, _optionnel_)
Cette clé indique si le protocole DHCP doit être utilisé pour obtenir
une adresse IPv4 pour cette interface. Si cette clé est omise, DHCP sera
désactivé.

#### ``inet4.static.enabled`` (**bool**, _optionnel_)
Cette clé indique si cette interface doit être configurée à l'aide d'une
ou plusieurs adresses IPv4 statiques issues de la variable d'inventaire
[``my_network_inet4_addresses``][my_network_inet4_addresses]. Si cette
clé est omise, l'adressage statique sera désactivé sur cette interface.

### Configuration IPv6
Cette section documente les clés relatives à la configuration d'IPv6.

#### ``inet6.enabled`` (**bool**, _optionnel_)
Cette clé indique si la suite de protocoles IPv6 doit être activée
sur l'interface.  Si cette clé est omise, IPv6 sera désactivé.

#### ``inet6.autoconf.enabled`` (**bool**, _optionnel_)
Cette clé indique si le mécanisme d'autoconfiguration sans état d'IPv6
(SLAAC) doit être utilisé pour générer une adresse sur cette interface.
Si cette clé est omise, l'autoconfiguration sera désactivée.

#### ``inet6.static.enabled`` (**bool**, _optionnel_)
Cette clé indique si cette interface doit être configurée à l'aide d'une
ou plusieurs adresses IPv6 statiques issues de la variable d'inventaire
[``my_network_inet6_addresses``][my_network_inet6_addresses]. Si cette
clé est omise, l'adressage statique sera désactivé sur cette interface.