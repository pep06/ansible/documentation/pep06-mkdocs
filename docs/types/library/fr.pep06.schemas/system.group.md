---
title: system.group  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# Type de données ``fr.pep06.schemas.system.group``

Ce type de données décrit un **groupe d'utilisateurs local** d'un
système d'exploitation.

## Description
Ce type de données permet de représenter un groupe d'utilisateurs local
au sein d'un système d'exploitation.

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.system.group
name: <str>
description: <str?>
tags: <str[]?>
gid: <int?>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``name`` (**str**, _obligatoire_)
Le nom associé au groupe d'utilisateur. Il s'agit ordinairement d'une
chaîne courte, sans caractères spéciaux (``/^[a-zA-Z0-9_-]+$/``).

#### ``description`` (**str**, _optionnel_)
Une courte description associée au groupe d'utilisateurs.

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées au groupe d'utilisateurs.
Ces étiquettes permettent aux rôles de filtrer la liste des groupes afin
de cibler uniquement ceux qui les intéressent.

#### ``gid`` (**int**, _optionnel_)
Une valeur de gid statique. Si cette clé est présente, le groupe se
verra affecter le gid spécifié sur les systèmes Unix. Si elle est omise,
le gid sera aléatoirement choisi.
