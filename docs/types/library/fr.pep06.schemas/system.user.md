---
title: system.user  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[pki.ssh.keypair]: /types/library/fr.pep06.schemas/pki.ssh.keypair

# Type de données ``fr.pep06.schemas.system.user``

Ce type de données décrit un **utilisateur local** d'un système
d'exploitation.

## Description
Ce type de données permet de représenter un utilisateur local au sein
d'un système d'exploitation.

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.system.user
name: <str>
password: <str?>
description: <str?>
tags: <str[]?>
uid: <int?>
groups:
  primary: <str?>
  supplementary: <str[]?>
home:
  path: <str?>
ssh:
  authorized_keys: <pki.ssh.keypair[]?>
  exclusive_keys:  <pki.ssh.keypair[]?>
  forbidden_keys:  <pki.ssh.keypair[]?>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``name`` (**str**, _obligatoire_)
Le nom d'utilisateur associé à l'utilisateur. Il s'agit ordinairement
d'une chaîne courte, sans caractères spéciaux (``/^[a-zA-Z0-9_-]+$/``).

#### ``password`` (**str**, _optionnel_, _chiffré_)
Le mot de passe associé à l'utilisateur, chiffré via Ansible Vault.
Les rôles gérant les comptes utilisateurs mettront à jour le mot de
passe de l'utilisateur afin qu'il corresponde à la valeur de cette clé.

Si cette clé est présente et vaut ``null``, la connexion par mot de
passe sera désactivée, que ce soit pour un nouvel utilisateur ou pour
un utilisateur existant.

Si cette clé est omise et que l'utilisateur n'existe pas, il sera créé
avec la connexion par mot de passe désactivée. Si cette clé est omise
et que l'utilisateur existe déjà, aucune modification ne sera apportée
à son mot de passe.

#### ``description`` (**str**, _optionnel_)
Une courte description associée à l'utilisateur.

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées à l'utilisateur. Ces
étiquettes permettent aux rôles de filtrer la liste des utilisateurs
afin de cibler uniquement ceux qui les intéressent.

#### ``uid`` (**int**, _optionnel_)
Une valeur d'uid statique. Si cette clé est présente, l'utilisateur se
verra affecter l'uid spécifié sur les systèmes Unix. Si elle est omise,
l'uid sera aléatoirement choisi.

#### ``groups.primary`` (**str**, _optionnel_)
Cette clé permet de définir statiquement le nom du groupe principal de
l'utilisateur sur les systèmes Unix. Si cette cle est omise, il est
attendu que le groupe principal de l'utilisateur porte le même nom que
lui.

#### ``groups.supplementary`` (**str[]**, _optionnel_)
Une liste optionnelle de noms de groupes dont l'utilisateur doit être
membre, en plus de son groupe principal.

#### ``home.path`` (**str**, _optionnel_)
Un chemin d'accès statique vers le répertoire de l'utilisateur. Si
cette clé est omise, les valeurs par défaut seront utilisées.

#### ``ssh.authorized_keys`` (**pki.ssh.keypair[]**, _optionnel_)
Liste de clés publiques SSH devant être présentes dans le fichier
``authorized_keys`` de l'utilisateur. Chaque entrée de cette liste est
un ``dict``, instance du type [pki.ssh.keypair][pki.ssh.keypair].

#### ``ssh.exclusive_keys`` (**pki.ssh.keypair[]**, _optionnel_)
Liste de clés publiques SSH devant être présentes dans le fichier
``authorized_keys`` de l'utilisateur, à l'exclusion de toute autre.
Chaque entrée de cette liste est un ``dict``, instance du type
[pki.ssh.keypair][pki.ssh.keypair].

Si cette clé est définie, toutes les clés publiques ne faisant pas
partie de cette liste seront supprimées du fichier des clés autorisées.

Si cette clé pointe vers une liste vide, la liste des clés autorisées
sera intégralement purgée.

#### ``ssh.forbidden_keys`` (**pki.ssh.keypair[]**, _optionnel_)
Liste de clés publiques SSH devant être absentes du fichier
``authorized_keys`` de l'utilisateur. Chaque entrée de cette liste est
un ``dict``, instance du type [pki.ssh.keypair][pki.ssh.keypair].