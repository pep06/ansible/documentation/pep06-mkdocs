---
title: network.address  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[my_network_interfaces]: /inventory/variables/library/my/my_network_interfaces/
[network.interface]: ../network.interface/

# Type de données ``fr.pep06.schemas.network.address``

Ce type de données décrit une **adresse** au sein d'un réseau IP.

## Description
Ce type de données permet de représenter une adresse au sein d'un réseau
IPv4 ou IPv6.

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.network.address
uid: <str>
tags: <str[]?>
description: <str?>
interface: <str>
protocol: <str(inet4|inet6)>
address: <str>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``uid`` (**str**, _obligatoire_)
Un identifiant unique associé à l'adresse IP. Cet identifiant pourra
être utilisé pour référencer l'adresse à partir d'autres contextes.
Cet identifiant doit être court, et uniquement composé de caractères
alphanumériques, underscore et tirets (``/^[a-zA-Z0-9_-]+$/``).

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées à l'adresse IP. Ces
étiquettes permettent aux rôles de filtrer la liste des adresses IP
afin de cibler uniquement celles qui les intéressent. Le tableau
ci-dessous liste quelques étiquettes couramment affectées aux
adresses:

| Etiquette | Description                                                |
| ----------| ---------------------------------------------------------- |
| ``main``  | Indique qu'il s'agit de l'adresse principale de l'hôte     |
| ``mgmt``  | Indique qu'il s'agit de l'adresse de supervision de l'hôte |

#### ``description`` (**str**, _optionnel_)
Une courte description de l'adresse IP.

#### ``interface`` (**str**, _obligatoire__)
Cette clé indique l'interface réseau à laquelle l'adresse IP est
affectée. L'[interface cible][network.interface] est désignée par son
identifiant unique, qui correspond à la valeur de sa clé ``uid``.

Lorsque l'identifiant unique de l'interface est variable, ou ne peut pas
être connu à l'avance, la valeur de cette clé peut être affectée
dynamiquement via un jeu de filtres Jinja appliqués à la variable
[``my_network_interfaces``][my_network_interfaces].

#### ``protocol`` (**str**, _obligatoire_)
Cette clé indique le protocole de l'adresse IP. Elle peut prendre une
des valeurs suivantes:

| Valeur    | Description                             |
| --------- | --------------------------------------- |
| ``inet4`` | Indique qu'il s'agit d'une adresse IPv4 | 
| ``inet6`` | Indique qu'il s'agit d'une adresse IPv6 | 

#### ``address`` (**str**, _obligatoire_)
Une adresse IPv4 ou IPv6 suivie du caractère slash (``/``) puis d'une
longueur de préfixe. Cette notation est standard pour les adresses IPv6.
En contexte IPv4, cela revient à exprimer l'adresse en notation CIDR.