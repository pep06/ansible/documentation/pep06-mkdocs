---
title: storage.encryption.options  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[storage.volume]: ../storage.volume/

# Type de données ``fr.pep06.schemas.storage.encryption.options``

Ce type de données décrit un ensemble d'**options de chiffrement**
applicables à un [volume de stockage][storage.volume].

## Description

Ce type de données permet de représenter l'ensemble des options
contrôlant le chiffrement d'un volume de stockage. À l'origine, ces
options étaient intégrées au type [``storage.volume``][storage.volume].
Elles font désormais l'objet d'un type séparé afin de pouvoir être
utilisées comme paramètres d'invocation pour les rôles configurant le
chiffrement.

## Structure attendue

Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.storage.encryption.options
algorithms:
  encryption: <str(none|aes-ecb|aes-cbc-plain|aes-cbc-essiv-md5|
                   aes-cbc-essiv-sha256|aes-xts-plain|
                   blowfish-cbc-plain)?>
  integrity: <str(none|md5|sha1|sha256|sha512)?>
  key_length: <int?>
secrets:
  keyfile: <str?>
features:
  trim_propagation:
    enabled: <bool?>
  keyfile_generation:
    enabled: <bool?>
    length: <int?>
````

### Clés communes

Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``algorithms.encryption`` (**str**, _optionnel_)

Cette clé indique l'algorithme de chiffrement à utiliser pour chiffrer
le volume. Cette clé accepte une des valeurs suivantes:

| Valeur                   | Description                              |
| ------------------------ | ---------------------------------------- |
| ``none``                 | Aucun algorithme (_passthrough_)         |
| ``aes-ecb``              | AES-ECB                                  |
| ``aes-cbc-plain``        | AES-CBC in plain mode                    |
| ``aes-cbc-essiv-md5``    | AES-CBC in ESSIV mode with MD5 hashes    |
| ``aes-cbc-essiv-sha256`` | AES-CBC in ESSIV mode with SHA256 hashes |
| ``aes-xts-plain``        | AES-XTS in plain mode                    |
| ``blowfish-cbc-plain``   | Blowfish-CBC in plain mode               |

???+ info "Support limité"
    Les valeurs supportées, ainsi que la valeur par défaut, diffèrent
    en fonction des plateformes.

#### ``algorithms.integrity`` (**str**, _optionnel_)

Cette clé indique l'algorithme de hachage à utiliser pour contrôler
l'intégrité (authentifier) le contenu du volume. Habituellement,
l'authentification des données chiffrées est désactivée pour des raisons
de performance et de coût de stockage, les hachages pouvant consommer
jusqu'à 10% de l'espace d'un volume. Cette clé accepte une des valeurs
suivantes:

| Valeur     | Description                                       |
| -----------| ------------------------------------------------- |
| ``none``   | Aucun algorithme (Pas de contrôle de l'intégrité) |
| ``md5``    | HMAC-MD5                                          |
| ``sha1``   | HMAC-SHA1                                         |
| ``sha256`` | HMAC-SHA256                                       |
| ``sha512`` | HMAC-SHA512                                       |

???+ info "Support limité"
    Les valeurs supportées, ainsi que la valeur par défaut, diffèrent
    en fonction des plateformes.

#### ``akgorithms.key_length`` (**int**, _optionnel_)

Cette clé permet de spécifier la longueur des clés de chiffrement
qui seront utilisées par l'algorithme de chiffrement pour encoder les
données du volume. Les valeurs admises diffèrent en fonction
de l'algorithme de chiffrement utilisé. Le tableau ci-dessous donne
quelques indications de valeurs légales:

| Algorithme               | Longueurs de clés                   |
| ------------------------ | ----------------------------------- |
| ``aes-ecb``              | ``128``, ``192``, ``256``           |
| ``aes-cbc-plain``        | ``128``, ``192``, ``256``           |
| ``aes-cbc-essiv-md5``    | ``128``                             |
| ``aes-cbc-essiv-sha256`` | ``256``                             |
| ``aes-xts-plain``        | ``256``, ``512``                    |
| ``blowfish-cbc-plain``   | ``128 + n * 32`` avec ``n=[0..10]`` |

Si cette clé est omise, la valeur par défaut pour l'algorithme de
chiffrement sélectionné sera utilisée.

#### ``secrets.keyfile`` (**str**, _optionnel_)

Le chemin d'accès au fichier de clé à utiliser pour chiffrer la clé
principale du volume.

#### ``features.trim_propagation.enabled`` (**bool**, _optionnel_)

Cette clé permet de spécifier si les commandes TRIM doivent être
propagées du volume chiffré au périphérique de stockage. La propagation
de ces commandes peut avoir pour conséquence de révéler à un attaquant
des informations concernant l'utilisation de l'espace de stockage au
sein du volume. La criticité de ces informations fait débat. Cette clé
peut prendre l'une des valeurs suivantes:

| Valeur                 | Description                        |
| ---------------------- | ---------------------------------- |
| **``false`` (défaut)** | Ne pas propager les commandes TRIM |
| ``true``               | Propager les commandes TRIM        |

Si cette clé n'est pas spécifiée, les commandes TRIM ne seront pas
propagées au volume de stockage sous-jacent.

#### ``features.keyfile_generation.enabled`` (**str**, _optionnel_)

Si cette clé est présente et vaut ``true``, la génération automatique
d'un fichier de clé pour le volume sera activée. Charge aux rôles
d'implémenter cette fonctionnalité. Cette clé peut prendre une des
valeurs suivantes:

| Valeur                 | Description                                     |
| ---------------------- | ----------------------------------------------- |
| **``false`` (Défaut)** | Pas de génération automatique du fichier de clé |
| ``true``               | Génération automatique du fichier de clé        |

#### ``keyfile.generation.length`` (**str**, _optionnel_)

Lorsque la génération automatique des fichiers de clés est activée,
cette clé permet d'indiquer la longueur des fichiers à générer. Cette
clé accepte une valeur entière supérieure à zéro sans unité explicite.
L'unité implicite est le kilo-octet.
