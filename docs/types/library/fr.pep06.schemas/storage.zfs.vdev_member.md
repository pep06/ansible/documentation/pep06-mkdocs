---
title: storage.zfs.vdev_member  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[storage.zfs.pool]: ../storage.zfs.pool/
[storage.zfs.vdev]: ../storage.zfs.vdev/

# Type de données ``fr.pep06.schemas.storage.zfs.vdev_member``

Ce type de données décrit un **membre d'un vdev d'un pool de stockage
ZFS**.

## Description
Ce type de données permet de représenter un périphérique de stockage
ou une partition membre d'un [périphérique virtuel][storage.zfs.vdev],
lui-même inclus dans un [pool de stockage ZFS][storage.zfs.pool].

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.storage.zfs.vdev_member
device: <str>
partition: <str?>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``device`` (**str**, _obligatoire_)
L'identifiant unique du périphérique de stockage membre du vdev.
La valeur référencée par cette clé doit correspondre à celle de la
propriété ``uid`` du périphérique de stockage cible.

#### ``partition`` (**str**, _optionnel_)
L'identifiant unique d'une partition sur le périphérique de stockage
désigné par la clé ``device``. La valeur référencée par cette clé doit
correspondre à celle de la propriété ``uid`` de la partition cible sur
le périphérique spécifié.

Si cette clé est spécifiée, seule la partition ciblée sera ajoutée
comme membre de vdev. Si elle est omise, lé périphérique entier sera
utilisé.