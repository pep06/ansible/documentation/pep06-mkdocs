---
title: network.route  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[my_network_gateways]: /inventory/variables/library/my/my_network_gateways/
[network.gateway]: ../network.gateway/

# Type de données ``fr.pep06.schemas.network.route``

Ce type de données décrit une **route** au sein d'une table de routage
IP.

## Description
Ce type de données permet de représenter une route au sein d'une table
de routage IP. Ce type permet la représentation de routes IPv4 ou
IPv6, la version du protocole étant déterminée par les rôles au moment
de l'exécution. Il est néanmoins d'usage d'étiqueter chaque route avec
la version du protocole utilisé.

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.network.route
uid: <str>
tags: <str[]?>
description: <str?>
destination: <str>
gateway: <str?>
metric: <int?>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``uid`` (**str**, _obligatoire_)
Un identifiant unique associé à la route. Cet identifiant pourra
être utilisé pour référencer la route à partir d'autres contextes.
Cet identifiant doit être court, et uniquement composé de caractères
alphanumériques, underscore et tirets (``/^[a-zA-Z0-9_-]+$/``).

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées à la route. Ces
étiquettes permettent aux rôles de filtrer la liste des routes afin
de cibler uniquement celles qui les intéressent. Le tableau ci-dessous
liste quelques étiquettes couramment affectées aux routes:

| Etiquette   | Description                  |
| ----------- | ---------------------------- |
| ``default`` | Indique une route par défaut |
| ``inet4``   | Indique une route IPv4       |
| ``inet6``   | Indique une route IPv6       |

#### ``description`` (**str**, _optionnel_)
Une courte description associée à cette route.

#### ``destination`` (**str**, _obligatoire_)
L'adresse du sous-réseau IPv4 ou IPv6 de destination.

#### ``gateway`` (**str**, _optionnel_)
La passerelle à utiliser pour rejoindre le sous-réseau de destination.
La [passerelle cible][network.gateway] est désignée par son identifiant
unique, qui correspond à la valeur de sa clé ``uid``.

Lorsque l'identifiant unique de la passerelle est variable, ou ne peut
pas être connu à l'avance, la valeur de cette clé peut être affectée
dynamiquement via un jeu de filtres Jinja appliqués à la variable
[``my_network_gateways``][my_network_gateways].

#### ``metric`` (**int**, _optionnel_)
La métrique associée à la route.
