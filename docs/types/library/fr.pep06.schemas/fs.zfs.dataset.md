---
title: fs.zfs.dataset  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[storage.zfs.pool]: ../storage.zfs.pool/

# Type de données ``fr.pep06.schemas.fs.zfs.dataset``

Ce type de données décrit un **jeu de données ZFS**.

## Description
Ce type de données permet de représenter un jeu de données ZFS (ou
_ZFS dataset_) au sein d'un [pool de stockage ZFS][storage.zfs.pool].

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.fs.zfs.dataset
uid: <str>
name: <str>
description: <str?>
managed: <bool?>
tags: <str[]?>
pool: <str>
properties: <dict?>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``uid`` (**str**, _obligatoire_)
Cette clé permet de définir un identifiant unique associé au jeu de
données ZFS. Cet identifiant sera uniquement utilisé à l'intérieur
d'Ansible, et n'apparaîtra à aucun moment sur le système cible.

#### ``name`` (**str**, _obligatoire_)
Le nom complet du jeu de données ZFS. Cette clé accepte une valeur
chaîne uniquement constituée de caractères alphanumériques,
underscore (``_``) , tiret (``-``) et slash (``/``), ce dernier
caractère faisant office de séparateur des niveaux de la hiérarchie des
jeux de données.

???+ warning "Omission du nom du pool ZFS parent"
    Le premier segment du nom d'un jeu de données correspond toujours
    au nom de son pool de stockage ZFS parent. L'identité du pool parent
    étant déjà fournie par la clé ``pool``, **le nom du pool ZFS parent
    doit être omis** de la valeur référencée par cette clé. Les rôles
    consommant cette ressource reconstitueront le nom complet en
    agrégeant la définition du pool et du jeu de données.

    Par conséquent, cette clé peut légalement référencer une chaîne
    vide, ce qui revient à décrire le jeu de données racine du pool
    ZFS parent. 

#### ``description`` (**str**, _optionnel_)
Cette clé permet d'associer une courte description au jeu de données.

#### ``managed`` (**bool**, _optionnel_)
Ce drapeau indique si le jeu de données ZFS doit ou non être géré par
Ansible. Il peut prendre les valeurs suivantes:

| Valeur                | Description                                                       |
| --------------------- | ----------------------------------------------------------------- |
| **``true`` (défaut)** | Le jeu de données ZFS est géré par Ansible                        |
| ``false``             | Le jeu de données ZFS n'est pas géré par Ansible et sera ignoré   |

#### ``tags`` (**str[]**, _optionnel_)
Une liste d'étiquettes arbitraires associées au jeu de données. Ces
étiquettes permettent aux rôles de filtrer la liste des jeux de données
ZFS afin de cibler uniquement ceux qui les intéressent.

#### ``pool`` (**str**, _obligatoire_)
L'identifiant unique du [pool ZFS][storage.zfs.pool] auquel appartient
ce jeu de données. La chaîne de caractères référencée par cette clé
doit corresponde à la valeur de la clé ``uid`` du pool parent.

#### ``properties`` (**dict**, _optionnel_)
Une liste optionnelle de propriétés associées au jeu de données ZFS.
Cette clé référence un dictionnaire, mappant un nom de propriété
(``str``) à la valeur de cette propriété (``str``). La liste complète
des propriétés supportées par les jeux de données ZFS varie selon la
version de ZFS utilisée, et est consultable via le manuel.