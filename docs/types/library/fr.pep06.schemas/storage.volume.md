---
title: storage.volume  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[storage.partition]:          ../storage.partition/
[storage.encryption.options]: ../storage.encryption.options/

# Type de données ``fr.pep06.schemas.storage.volume``

Ce type de données décrit un **volume de stockage**.

## Description

Ce type de données permet de représenter un volume de stockage au sens
large, tel qu'un disque dur, une de ses partitions ou un volume chiffré.

## Structure attendue

Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.storage.volume
uid: <str>
label: <str?>
description: <str?>
type: <str(disk|encrypted|partition)>
tags: <str[]?>
managed: <bool?>
scheme: <str(none|gpt|mbr)?>
disk:
  chassis: <str?>
  tray: <str>
  type: <str(hdd|ssd|virtual)>
  identifier:
    value: <str>
    class: <str(id|dev|path|uuid)>
encrypted:
  parent: <str>
  options: <storage.encryption.options?>
partition:
  parent: <str>
  index: <int?>
  start: <str?>
  end: <str?>
  encryption: <storage.encryption.options?>
````

### Clés communes

Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``uid`` (**str**, _obligatoire_)

Un identifiant unique associé au volume de stockage. Cet identifiant
pourra être utilisé pour référencer le volume à partir d'autres
contextes. Cet identifiant doit être court, et uniquement composé de
caractères alphanumériques, underscore et tirets
(``/^[a-zA-Z0-9_-]+$/``).

#### ``label`` (**str**, _optionnel_)

Une étiquette optionnelle associée au volume. Cette étiquette sera
utilisée pour nommer le volume. Le tableau ci-dessous indique la manière
dont l'étiquette sera utilisée en fonction du type de volume:

| Type de volume | Usage de l'étiquette             |
|----------------|----------------------------------|
| ``disk``       | Aucun                            |
| ``partition``  | Étiquette de partition           |
| ``encrypted``  | Nom symbolique du volume chiffré |

#### ``description`` (**str**, _optionnel_)

Une courte description associée à ce volume de stockage.

#### ``type`` (**str**, _obligatoire_)

Cette clé indique le type du volume de stockage. Elle peut prendre une
des valeurs suivantes:

| Valeur        | Description                                |
|---------------|--------------------------------------------|
| ``disk``      | Périphérique de stockage                   |
| ``encrypted`` | Volume chiffré superposé à un autre volume |
| ``partition`` | Partition issue d'un autre volume          |

#### ``tags`` (**str[]**, _optionnel_)

Une liste d'étiquettes arbitraires associées au volume de stockage. Ces
étiquettes permettent aux rôles de filtrer la liste des volumes de
stockage afin de cibler uniquement ceux qui les intéressent. Le tableau
ci-dessous liste les étiquettes couramment utilisées:

| Valeur     | Description                               |
|------------|-------------------------------------------|
| ``backup`` | Périphérique dédié aux sauvegardes        |
| ``cache``  | Périphérique dédié au cache de données    |
| ``data``   | Périphérique dédié au stockage de données |
| ``system`` | Périphérique dédié au système de l'hôte   |

#### ``managed`` (**bool**, _optionnel_)

Ce drapeau indique si le volume de stockage doit être géré par Ansible.
Cette clé peut prendre les valeurs suivantes:

| Valeur                 | Description                                   |
|------------------------|-----------------------------------------------|
| **``false`` (défaut)** | Le périphérique n'est pas géré et sera ignoré |
| ``true``               | Le périphérique est géré par Ansible          |

#### ``scheme`` (**str**, _optionnel_)

Cette clé permet de spécifier le schéma de partitionnement associé au
volume de stockage. La valeur par défaut (``none``) indique que le
volume ne doit pas être partitionné. Cette clé peut prendre une des
valeurs suivantes:

| Valeur                | Description                                   |
|-----------------------|-----------------------------------------------|
| **``none`` (défaut)** | Ne pas partitionner le volume                 |
| ``gpt``               | Partitionner le volume au moyen du schéma GPT |
| ``mbr``               | Partitionner le volume au moyen du schéma MBR |

### Clés pour volumes de type disque

Les clés documentées dans cette section sont uniquement applicables
aux volumes correspondant à des périphériques de stockage de masse,
c'est-à-dire dont la clé ``type`` vaut ``disk``.

#### ``disk.chassis`` (**str**, _optionnel_)

Une lettre majuscule identifiant le chassis auquel fait référence
l'emplacement désigné par la clé ``tray``. Si le serveur ne comporte
qu'un seul chassis, cette clé peut être omise.

#### ``disk.tray`` (**str**, _obligatoire_)

Identifiant unique de tiroir à disque dur occupé par le périphérique
de stockage. Cette clé permet d'associer un emplacement de tiroir ou de
cage interne à un périphérique de stockage.

Bien qu'arbitraires, les identifiants de tiroirs sont ordinairement
construits sur la base du patron ``[(u|l|i)]<index>``, où:

- ``(u|l|i)`` est une lettre minuscule indiquant la nature de
  l'emplacement de stockage, à savoir:

  - ``u`` (_**u**pper_) pour un tiroir situé dans la partie haute
    du chassis.

  - ``l`` (_**l**ower_) pour un tiroir situé dans la partie basse
    du chassis.

  - ``i`` (_**i**nternal_) pour une cage à disque durs située à
    l'intérieur du chassis.

- ``<index>`` est un nombre entier sur deux chiffres indiquant le
  numéro de l'emplacement occupé par le périphérique. La numérotation
  est habituellement effectuée depuis 0, mais certains chassis sont
  numérotés à partir de 1. La règle d'or est de respecter la manière
  dont les emplacements sont physiquement étiquetés.

    ??? example "Exemples d'identifiants d'emplacements"
        - L'identifiant ``06`` cible le tiroir numéro 6
        - L'identifiant ``01`` cible le tiroir numéro 1
        - L'identifiant ``l00`` cible le tiroir numéro 0 du groupe
          de tiroirs le plus en hauteur.
        - L'identifiant ``i04`` cible l'emplacement numéro 4 d'une cage
          à disques durs interne.

#### ``disk.type`` (**str**, _obligatoire_)

Cette clé permet de spécifier le type du périphérique de stockage.
Elle peut prendre l'une des valeurs suivantes:

| Valeur      | Description                            |
|-------------|----------------------------------------|
| ``hdd``     | Périphérique de stockage mécanique ATA |
| ``ssd``     | Périphérique de stockage flash ATA     |
| ``virtual`` | Périphérique de stockage virtuel       |

#### ``disk.identifier.value`` (**str**, _obligatoire_)

Cette clé permet de définir l'identifiant unique associé au périphérique
de stockage sur le système hôte. La valeur de cette clé doit être
interprétée dans le contexte du référentiel de nommage spécifié par
la clé ``disk.identifier.class``.

#### ``disk.identifier.class`` (**str**, _obligatoire_)

Cette clé permet de spécifier la nature de l'identifiant unique de
périphérique de stockage indiqué par la clé ``disk.identifier.value``.
Cette clé peut prendre une des valeurs suivantes:

| Valeur   | Type d'identifiant      | Chemin d'accès sous Linux  |
|----------|-------------------------|----------------------------|
| ``id``   | Identifiant de matériel | ``/dev/disk/by-id/<id>``   |
| ``dev``  | Nom de périphérique     | ``/dev/<id>``              |
| ``path`` | Chemin d'accès matériel | ``/dev/disk/by-path/<id>`` |
| ``uuid`` | UUID de périphérique    | ``/dev/disk/by-uuid/<id>`` |

### Clés pour volumes chiffrés

Les clés documentées dans cette section sont uniquement applicables
aux volumes chiffrés, c'est-à-dire dont la clé ``type`` vaut
``encrypted``.

#### ``encrypted.parent`` (**str**, _obligatoire_)

Le volume parent dont est issu le volume chiffré. La valeur de cette clé
doit correspondre à celle de la clé ``uid`` du volume parent.

#### ``encrypted.options`` (**storage.encryption.options**, _optionnel_)

Un jeu facultatif d'options de chiffrement pour le volume chiffré.
Ces options sont définies sous la forme d'une structure de type ``dict``
constituant une instance du type [``storage.encryption.options``]
[storage.encryption.options]

### Clés pour volumes de type partition

Les clés documentées dans cette section sont uniquement applicables
aux volumes correspondant à des partitions de volumes  de stockage,
c'est-à-dire dont la clé ``type``vaut ``partition``.

#### ``partition.parent`` (**str**, _obligatoire_)

Le volume parent auquel appartient la partition. La valeur de cette clé
doit correspondre à celle de la clé ``uid`` du volume parent.

#### ``partition.index`` (**int**, _optionnel__)

Un nombre entier supérieur ou égal à 0 identifiant la place de la
partition dans la table de partitions du volume parent. Cette clé peut
être omise si le volume parent ne comporte qu'une seule partition. Dans
ce cas, la partition se verra assigner la valeur d'index ``0``.

#### ``partition.start`` (**str**, _optionnel_)

Emplacement LBA marquant le début de la partition. Cette clé attend une
valeur numérique, associée à une unité de stockage ou à de pourcentage
(``/^[09\.]+(KB|KiB|MB|MiB|GB|GiB|TB|TiB|%)$/``). Si cette clé est
omise, une valeur de décalage de 0 sera utilisée.

#### ``partition.end`` (**str**, _optionnel_)

Emplacement LBA marquant la fin de la partition. Cette clé attend une
valeur numérique, associée à une unité de stockage ou à de pourcentage
(``/^[09\.]+(KB|KiB|MB|MiB|GB|GiB|TB|TiB|%)$/``). Si cette clé est
omise, la valeur maximale autorisée pour le périphérique sera utilisée.

#### ``partition.encryption`` (**storage.encryption.options**, _optionnel_)

Si la partition doit être chiffrée, des options de chiffrement peuvent
être déclarées sous cette clé, sous la forme d'une instance du type
[``storage.encryption.options``][storage.encryption.options].
