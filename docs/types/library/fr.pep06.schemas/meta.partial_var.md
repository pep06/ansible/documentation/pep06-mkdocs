---
title: meta.partial_var  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# Type de données ``fr.pep06.schemas.meta.partial_var``

Ce type de données décrit une **variable d'inventaire partielle**
utilisée dans un inventaire Ansible.

## Description

Ce type de données permet de représenter les caractéristiques d'une
variable d'inventaire partielle. Ces caractéristiques sont ensuite
consommées par Ansible pour procéder à la consolidation de la variable
préalablement à l'exécution d'un _playbook_.

## Structure attendue

Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.meta.partial_var
name: <str>
regex: <str>
type: <str(dict|list)>
description: <str?>
requires: <str[]?>
````

### Clés communes

Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``name`` (**str**, _obligatoire_)

Le nom de la variable d'inventaire consolidée à générer à partir des
variables partielles.

#### ``regex`` (**str**, _obligatoire_)

Une expression régulière permettant d'identifier les variables
d'inventaire partielles à fusionner lors de la consolidation. Ansible
utilisera cette expression régulière pour filtrer la liste des
variables d'inventaire connues en vue d'obtenir la liste des variables
partielles à fusionner.

#### ``type`` (**str**, _obligatoire_)

Le type attendu de la variable consolidée. Cette clé peut prendre l'une
des deux valeurs suivantes:

| Valeur   | Description                                        |
|----------|----------------------------------------------------|
| ``dict`` | Si la variable consolidée est de type dictionnaire |
| ``list`` | Si la variable consolidée est de type liste        |

#### ``description`` (**str**, _optionnel_)

Une description associée à la variable d'inventaire.

#### ``requires`` (**str[]**, _optionnel_)

Une liste optionnelle de variables devant être définies **avant** la
consolidation de la variable courante. Cette clé permet de définir des
relations de dépendance entre variables partielles. Ces dépendances sont
pour le moment uniquement définies à des fins de documentation, et ne
sont pas vérifiées ni priorisées par Ansible.
