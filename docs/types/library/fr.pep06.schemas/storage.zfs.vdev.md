---
title: storage.zfs.vdev  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[storage.zfs.pool]:        ../storage.zfs.pool/
[storage.zfs.vdev_member]: ../storage.zfs.vdev_member/

# Type de données ``fr.pep06.schemas.storage.zfs.vdev``

Ce type de données décrit un **vdev de pool de stockage ZFS**.

## Description
Ce type de données permet de représenter un périphérique virtuel
(_vdev_) opérant au sein d'un [pool de stockage ZFS][storage.zfs.pool].

## Structure attendue
Ce type de données doit satisfaire à la structure suivante:

````yaml
---
# !fr.pep06.schemas.storage.zfs.vdev
type: <str(disk|mirror|raidz1|raidz2|raidz3|spare|log|cache)>
# !fr.pep06.schemas.storage.zfs.vdev_member[]
members: <storage.zfs.vdev_member[]>
````

### Clés communes
Les clés documentées dans cette section sont applicables à n'importe
quelle instance de ce type de données.

#### ``type`` (**str**, _obligatoire_)
Le type du vdev. Cette clé accepte une des valeurs suivantes:

| Valeur     | Description                                                      |
| ---------- | ---------------------------------------------------------------- |
| ``disk``   | Un seul volume sans redondance                                   |
| ``mirror`` | Au moins deux volumes avec redondance en mode miroir             |
| ``raidz1`` | Au moins trois volumes avec simple redondance par parité         |
| ``raidz2`` | Au moins quatre volumes avec double redondance par parité        |
| ``raidz2`` | Au moins quatre volumes avec double redondance par parité        |
| ``spare``  | Au moins un volume pour remplacement automatique en cas de panne |
| ``log``    | Au moins un volume dédié au journal d'intentions (ZIL)           |
| ``cache``  | Au moins un volume dédié au cache de second niveau (L2ARC)       |

Les vdevs de type ``file`` sont volontairement non-supportés.

#### ``members`` (**storage.zfs.vdev_member[]**, _obligatoire_)
La liste des périphériques, partitions ou fichiers membres du vdev.
Cette clé est obligatoire et doit comprendre au moins une entrée.
Chaque entrée référence un ``dict`` correspondant à une instance du type
[storage.zfs.vdev_member][storage.zfs.vdev_member].