---
title: path_backups_my  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``meta_path_backups_my`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès au répertoire de l'espace
de travail dédié aux sauvegardes de l'hôte courant.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès au répertoire dédié aux sauvegardes (représenté par la
variable ``meta_path_backups``) et le nom d'inventaire de l'hôte
courant, issu de la variable ``inventory_hostname``.

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    ---
    meta_path_backups_my: >-
      {{ meta_path_backups ~ '/' ~ inventory_hostname }}
    ````
