---
title: path_generators  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``meta_path_generators`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès au répertoire où sont
intallés les générateurs, qui sont un ensemble de modèles Jinja2
partagés par l'ensemble de l'espace de travail et permettant de générer
des structures de données communes consommées par divers rôles.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès au répertoire racine des dépôts d'utilitaires
(représenté par la variable ``meta_path_helpers``) et le nom du
sous-répertoire où est clôné le dépôt des générateurs (habituellement
nommé ``generators``).

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    ---
    meta_path_generators: "{{ meta_path_helpers ~ '/generators' }}"
    ````
