---
title: path_our  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``meta_path_our`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès complet au répertoire
de l'inventaire contenant les définitions de variables communes. Elle
est utilisée à divers autres endroits de l'inventaire, notamment pour
inclure le contenu de fichiers au moyen du plugin _file lookup_.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès absolu au répertoire racine de l'inventaire
(représenté par la variable ``inventory_dir``) et le chemin d'accès
relatif au répertoire ``groups_vars/all/our``, où sont définies les
variables communes.

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    ---
    meta_path_our: "{{ inventory_dir ~ '/group_vars/all/our' }}"
    ````
