---
title: path_my_own_pki_ssh_keypairs  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[meta_path_my_own]: ../meta_path_my_own/

# ``meta_path_my_own_pki_ssh_keypairs`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès complet aux fichiers de
l'inventaire stockant les paires de clés de chiffrement SSH propres à
l'hôte courant.

Cette variable facilite l'inclusion et le stockage de paires de clés de
chiffrement dans des fichiers externes.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès absolu au répertoire racine de l'inventaire dédié à
l'hôte courant (variable [``meta_path_my_own``][meta_path_my_own] et le
chemin relatif ``pki/ssh/keypairs``, qui constitue la racine du dépôt
de clés de chiffrement SSH pour cet hôte.

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    # <inventory_dir>/group_vars/all/meta/path/my/own/pki/ssh/keypairs.yml
    ---
    meta_path_my_own_pki_ssh_keypairs: >-
      {{ meta_path_my_own ~ '/pki/ssh/keypairs' }}
    ````
