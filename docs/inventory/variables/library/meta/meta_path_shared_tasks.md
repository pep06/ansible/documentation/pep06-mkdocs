---
title: path_shared_tasks  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``meta_path_shared_tasks`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès au répertoire où sont
stockés les fichiers de tâches partagés par l'ensemble des rôles. Ces
fichiers de tâches sont maintenus dans un dépôt d'utilitaires dédié.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès au répertoire racine des dépôts d'utilitaires
(représenté par la variable ``meta_path_helpers``) et le nom du
sous-répertoire où est clôné le dépôt des tâches partagées (
habituellement nommé ``tasks``).

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    ---
    meta_path_shared_tasks: "{{ meta_path_helpers ~ '/tasks' }}"
    ````
