---
title: partial_vars  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[partial_vars]: /inventory/variables/#variables-partielles
[meta.partial_var]: /types/library/fr.pep06.schemas/meta.partial_var

# ``meta_partial_vars`` (**dict**, _obligatoire_)

## Description

Cette métavariable contient la liste des [variables partielles]
[partial_vars] utilisées dans l'inventaire. Elle permet à Ansible de
procéder à la fusion des variables partielles avant l'exécution d'un
playbook.

## Type

Cette variable est de type liste.

## Structure

Chaque entrée de la liste est un dictionnaire (``dict``) constituant
une instance du type complexe [``fr.pep06.schemas.meta.partial_var``]
[meta.partial_var].

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant définit une variable consolidée
    ``my_consolidated_vars``, qui sera générée en fusionnant toutes les
    variables d'inventaires validant l'expression régulière
    ``/^my_consolidated_vars__.*$/``:

    ````yaml
    ---
    meta_partial_vars_:
      - name: my_consolidated_vars
        regex: "^my_consolidated_vars__.*$"
        type: list
    ````
