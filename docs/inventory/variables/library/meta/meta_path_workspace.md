---
title: path_workspace  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``meta_path_workspace`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès au répertoire racine de
l'espace de travail Ansible.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès absolu au répertoire racine de l'inventaire
(représenté par la variable ``inventory_dir``) et deux références
au niveau supérieur (``..``).

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    ---
    meta_path_workspace: "{{ inventory_dir ~ '/../../' }}"
    ````
