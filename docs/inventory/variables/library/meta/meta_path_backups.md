---
title: path_backups  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``meta_path_backups`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès au répertoire de l'espace
de travail dédié aux sauvegardes.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès au répertoire racine de l'espace de travail
(représenté par la variable ``meta_path_workspace``) et le nom du
répertoire de l'espace de travail dédié aux sauvegardes (habituellement
nommé ``backups``).

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    ---
    meta_path_backups: "{{ meta_path_workspace ~ '/backups' }}"
    ````
