---
title: path_helpers  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``meta_path_helpers`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès au répertoire où sont
intallés les _helper templates_.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès au répertoire racine de l'espace de travail
(représenté par la variable ``meta_path_workspace``) et le nom du
répertoire de l'espace de travail dédié aux _helper templates_
(habituellement nommé ``helpers``).

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    ---
    meta_path_helpers: "{{ meta_path_workspace ~ '/helpers' }}"
    ````
