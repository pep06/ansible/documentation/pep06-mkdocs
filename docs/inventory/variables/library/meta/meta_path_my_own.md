---
title: path_my_own  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``meta_path_my_own`` (**str**, _obligatoire_)

## Description

Cette métavariable définit le chemin d'accès complet au répertoire
de l'inventaire contenant les définitions de variables privées définies
pour l'hôte Ansible ciblé.

???+ note "A propos du nom de cette variable"
    L'usage des termes ``my_own`` au lieu de ``my`` a pour but de bien
    signifier que cette variable indique le chemin racine vers le
    répertoire d'inventaire **dédié à l'hôte**, c'est-à-dire enfant du
    sous-répertoire ``host_vars``. Le terme ``my`` est en effet trop
    ambigu, puisque de nombreuses variables d'inventaires privées
    portant ce préfixe sont définies au niveau de groupes d'inventaires.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Structure

Cette variable est une chaîne, habituellement construire en concaténant
le chemin d'accès absolu au répertoire racine de l'inventaire
(représenté par la variable ``inventory_dir``) et le chemin d'accès
relatif au répertoire ``host_vars/<inventory_hostname>/my``, où sont
définies les variables privées de l'hôte ciblé.

## Exemples

???+ example "Exemple standard"
    L'extrait de code suivant définit cette variable de manière
    standard:

    ````yaml
    ---
    meta_path_my_own: >-
      {{ inventory_dir ~ '/host_vars/' ~ inventory_hostname ~ '/my' }}
    ````
