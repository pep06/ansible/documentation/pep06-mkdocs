---
title: pki_ssh_keypairs(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[guidelines_pki]:  /guidelines/pki.md
[pki.ssh.keypair]: /types/library/fr.pep06.schemas/pki.ssh.keypair/

# ``our_pki_ssh_keypairs(__.*)?`` (**pki.ssh.keypair[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``our_pki_ssh_keypairs`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``our_pki_ssh_keypairs__``.

## Description

Cette variable stocke la liste des paires de clés de chiffrement SSH
publiées auprès de la totalité des hôtes de l'inventaire.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``) issu du type complexe
[``fr.pep06.schemas.pki.ssh.keypair``][pki.ssh.keypair].

## Usage

Chaque paire de clé contient obligatoirement une clé publique. Le type
de données prévoit la possibilité de lui associer une clé privée, mais
**cette pratique est prohibée pour des raisons de sécurité**.

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant définit une variable partielle stockant
    une clé publique appartenant à l'administrateur bernardm. La clé est
    lue et incluse à partir d'un fichier externe situé au même niveau
    que le fichier YAML et nommé identiquement. La clé privée est
    absente, conformément à la politique de sécurité appliquée dans
    nos inventaires.

    ````yaml
    # group_vars/all/our/pki/ssh/keypairs/_user/bernardm/ass-nbm-1903-01/id_ed25519.yml:
    ---
    # !fr.pep06.schemas.pki.ssh.keypair[]
    our_pki_ssh_keypairs__user_bernardm_ass-nbm-1903-01_id_ed25519:
      - uid: user_bernardm_ass-nbm-1903-01_id_ed25519
        tags: []
        algo: ed25519
        usage: user
        principal: bernardm@ass-nbm-1903-01.rmt.ros.intranet.pep06.fr
        public:
          openssh: >-
            {{
              lookup(
                'file',
                meta_path_our ~ '/pki/ssh/keypairs/' ~
                '_user/bernardm/ass-nbm-1903-01/id_ed25519.pub')
            }}
    ````

    Une fois la fusion des variables partielles réalisée, l'on
    obtiendra la variable consolidée ``our_pki_ssh_keypairs``
    suivante, qui sera publiquement accessible par l'ensemble des hôtes:

    ````yaml
    ---
    # !fr.pep06.schemas.pki.ssh.keypair[]
    our_pki_ssh_keypairs:
      - uid: user_bernardm_ass-nbm-1903-01_id_ed25519
        tags: []
        algo: ed25519
        usage: user
        principal: bernardm@ass-nbm-1903-01.rmt.ros.intranet.pep06.fr
        public:
          openssh: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINdLnpC5pCvkj+cMn8VH32i6GAzS684fFbUbP+rRip1r bernardm@ass-nbm-1903-01.rmt.ros.intranet.pep06.fr"
    ````

    Pour plus d'informations sur les conventions d'organisation des
    fichiers de clés publiques, consultez le [guide][guidelines_pki]
    qui leur est dédié.
