---
title: system_groups(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[system.group]: /types/library/fr.pep06.schemas/system.group/

# ``my_system_groups(__.*)?`` (**system.group[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``my_system_groups`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``my_system_groups__``.

## Description

Cette variable stocke la liste des groupes d'utilisateurs locaux définis
pour un hôte Ansible.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``) issu du type complexe
[``fr.pep06.schemas.system.group``][system.group].

## Usage

Chaque groupe est habituellement défini au moyen d'une variable
partielle dédiée, dont le suffixe reprend le nom du groupe.

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant définit le groupe ``backup`` au moyen
    d'une variable partielle:

    ````yaml
    # host_vars/<ansible_host>/my/system/groups/_backup.yml:
    ---
    my_system_groups__backup:
      - name: backup
        description: Backup identities
    ````

    Une fois la fusion des variables partielles réalisée, l'on
    obtiendra la variable consolidée ``my_system_groups`` suivante:

    ````yaml
    ---
    my_system_groups:
      - name: backup
        description: Backup identities
    ````
