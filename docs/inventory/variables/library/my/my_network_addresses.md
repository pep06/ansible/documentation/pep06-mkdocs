---
title: network_addresses(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[network.address]: /types/library/fr.pep06.schemas/network.address/

# ``my_network_addresses(__.*)?`` (**network.address[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``my_network_addresses`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``my_network_addresses__``.

## Description

Cette variable stocke la liste des adresses IP connues d'un hôte
Ansible.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``), correspondant à une instance du type de données
[``fr.pep06.schemas.network.address``][network.address].

## Usage

Cette liste contient habituellement la liste de toutes les adresses
IP assignées aux interfaces réseau d'un hôte Ansible. Chaque entrée de
la liste représente une seule adresse IPv4 ou IPv6.

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant représente une adresse IPv4 ``main4``,
    affecté à la première interface dont la clé ``zone`` vaut ``MAIN``:

    ````yaml
    # host_vars/<ansible_host>/my/network/addresses/_main4.yml:
    ---
    my_network_addresses__main4:
    - uid: main4
      tags: ['main', 'mgmt']
      description: Proxmox VE administration address
      protocol: inet4
      interface: "{{ my_network_interfaces
        | selectattr('zone', 'defined')
        | selectattr('zone', 'equalto', 'MAIN')
        | map(attribute='uid')
        | list
        | first
      }}"
      address: 10.6.12.194/23
    ````

    Une fois la fusion des variables partielles réalisée, l'on obtiendra
    la variable consolidée ``my_network_addresses`` suivante:

    ````yaml
    ---
    my_network_addresses:
    - uid: main4
      tags: ['main', 'mgmt']
      description: Proxmox VE administration address
      protocol: inet4
      interface: "{{ my_network_interfaces
        | selectattr('zone', 'defined')
        | selectattr('zone', 'equalto', 'MAIN')
        | map(attribute='uid')
        | list
        | first
      }}"
      address: 10.6.12.194/23
    ````
