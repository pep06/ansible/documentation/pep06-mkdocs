---
title: pki_ssh_keypairs(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[guidelines_pki]:  /guidelines/pki.md
[pki.ssh.keypair]: /types/library/fr.pep06.schemas/pki.ssh.keypair/
[our_pki_ssh_keypairs]: ../../our/our_pki_ssh_keypairs/

# ``my_pki_ssh_keypairs(__.*)?`` (**pki.ssh.keypair[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``my_pki_ssh_keypairs`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``my_pki_ssh_keypairs__``.

## Description

Cette variable stocke la liste des paires de clés de chiffrement SSH
publiées auprès d'un groupe d'hôtes ou d'un hôte unique. Elle est
similaire à la variable d'inventaire publique [``our_pki_ssh_keypairs``]
[our_pki_ssh_keypairs], mais d'une portée plus restreinte.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``) issu du type complexe
[``fr.pep06.schemas.pki.ssh.keypair``][pki.ssh.keypair].

## Usage

Chaque paire de clé contient obligatoirement une clé publique. Le type
de données prévoit la possibilité de lui associer une clé privée, mais
**cette pratique est prohibée pour des raisons de sécurité**.

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant définit une variable partielle stockant
    une clé publique appartenant à un hôte. La clé est lue et incluse à
    partir d'un fichier externe situé au même niveau que le fichier YAML
    et nommé identiquement. La clé privée est absente, conformément à la
    politique de sécurité appliquée dans nos inventaires.

    ````yaml
    # host_vars/testhost/my/pki/ssh/keypairs/_host_/testhost/id_rsa.yml:
    ---
    # !fr.pep06.schemas.pki.ssh.keypair[]
    my_pki_ssh_keypairs__host_testhost_id_rsa:
      - uid: host_testhost_id_rsa
        tags: []
        algo: rsa
        usage: host
        principal: testhost
        public:
          openssh: >-
            {{
              lookup(
                'file',
                inventory_dir ~
                'host_vars/testhost/my/pki/ssh/keypairs/' ~
                '_host_/testhost/id_rsa.pub')
            }}
    ````

    Une fois la fusion des variables partielles réalisée, l'on
    obtiendra la variable consolidée ``my_pki_ssh_keypairs``
    suivante, qui sera publiquement accessible par l'ensemble des hôtes:

    ````yaml
    ---
    # !fr.pep06.schemas.pki.ssh.keypair[]
    my_pki_ssh_keypairs:
      - uid: host_testhost_id_rsa
        tags: []
        algo: rsa
        usage: host
        principal: testhost
        public:
          openssh: "ssh-rsa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx testhost"
    ````

    Pour plus d'informations sur les conventions d'organisation des
    fichiers de clés publiques, consultez le [guide][guidelines_pki]
    qui leur est dédié.
