---
title: system_users(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[system.user]: /types/library/fr.pep06.schemas/system.user/

# ``my_system_users(__.*)?`` (**system.user[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``my_system_users`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``my_system_users__``.

## Description

Cette variable stocke la liste des utilisateurs locaux définis pour un
hôte Ansible.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``) issu du type complexe
[``fr.pep06.schemas.system.user``][system.user].

## Usage

Chaque utilisateur est habituellement défini au moyen d'une variable
partielle dédiée, dont le suffixe reprend le nom d'utilisateur.

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant définit l'utilisateur Bénédicte TARTEMPION
    au moyen d'une variable partielle:

    ````yaml
    # host_vars/<ansible_host>/my/system/users/_tartempionb.yml:
    ---
    my_system_users__tartempionb:
      - name: tartempionb
        description: Bénédicte TARTEMPION
        password: "MySuperSecurePa55w0rd"
        groups:
          supplementary: ['accounting', 'sales']
    ````

    Une fois la fusion des variables partielles réalisée, l'on
    obtiendra la variable consolidée ``my_system_users`` suivante:

    ````yaml
    ---
    my_system_users:
      - name: tartempionb
        description: Bénédicte TARTEMPION
        password: "MySuperSecurePa55w0rd"
        groups:
          supplementary: ['accounting', 'sales']
    ````
