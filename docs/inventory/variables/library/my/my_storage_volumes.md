---
title: storage_volumes(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[storage.volume]: /types/library/fr.pep06.schemas/storage.volume/

# ``my_storage_volumes(__.*)?`` (**storage.volume[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``my_storage_volumes`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``my_storage_volumes__``.

## Description

Cette variable stocke la liste des périphériques de stockage associés
à un hôte Ansible.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``) issu du type complexe
[``fr.pep06.schemas.storage.volume``][storage.volume].

## Usage

Chaque périphérique est habituellement défini au moyen d'une variable
partielle dédiée, dont le suffixe reprend son identifiant d'emplacement.

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant définit un périphérique de stockage au
    moyen d'une variable partielle dont le suffixe reprend son
    identifiant d'emplacement:

    ````yaml
    # host_vars/<ansible_host>/my/storage/devices/_tray_01.yml:
    ---
    # !fr.pep06.schemas.storage.volume[]
    my_storage_volumes__tray_01:
      - uid: ata-INTEL_SSDSC2CW120A3_CVCV3343013T120BGN
        uid_type: dev_id
        description: System disk
        type: ssd
        tray: "01"
        managed: false
    ````

    Une fois la fusion des variables partielles réalisée, l'on
    obtiendra la variable consolidée ``my_storage_volumes`` suivante:

    ````yaml
    ---
    my_storage_volumes:
      - uid: ata-INTEL_SSDSC2CW120A3_CVCV3343013T120BGN
        uid_type: dev_id
        description: System disk
        type: ssd
        tray: "01"
        managed: false
    ````
