---
title: network_gateways(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[network.gateway]: /types/library/fr.pep06.schemas/network.gateway/

# ``my_network_gateways(__.*)?`` (**network.gateway[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``my_network_gateways`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``my_network_gateways__``.

## Description

Cette variable stocke la liste des passerelles IP connues d'un hôte
Ansible.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``), correspondant à une instance du type de données
[``fr.pep06.schemas.network.gateway``][network.gateway].

## Usage

Cette liste contient habituellement la liste de toutes les passerelles
IP présentes sur les sous-réseaux auxquels l'hôte est relié. Chaque
passerelle étant limitée à une seule adresse par protocole IP, une
passerelle reliée à plusieurs sous-réseaux (routeur) sera
potentiellement décrite par plusieurs entrées distinctes.

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant décrit la passerelle ``ros-mtl-gwi-01``,
    qui correspond au routeur interne mutualisé de Rossetti, au moyen
    d'une variable partielle:

    ````yaml
    # host_vars/<ansible_host>/my/network/gateways/_ros-mtl-gwi-00.yml:
    ---
    my_network_gateways__ros-mtl-gwi-00:
      - uid: ros-mtl-gwi-00
        tags: ['default']
        description: Internal gateway
        addresses:
          inet4: '10.6.100.254'
    ````

    Une fois la fusion des variables partielles réalisée, l'on obtiendra
    la variable consolidée ``my_network_gateways`` suivante:

    ````yaml
    ---
    my_network_gateways:
      - uid: ros-mtl-gwi-00
        tags: ['default']
        description: Internal gateway
        addresses:
          inet4: '10.6.100.254'
    ````
