---
title: network_routes(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[my_network_gateways]: ../my_network_gateways/
[network.route]: /types/library/fr.pep06.schemas/network.route/

# ``network_routes(__.*)?`` (**network.route[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``my_network_routes`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``my_network_routes__``.

## Description

Cette variable stocke la liste des routes IP devant être définies dans
la table de routage d'un hôte Ansible.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``), correspondant à une instance du type de données
[``fr.pep06.schemas.network.route``][network.route].

## Usage

Cette liste contient la liste de toutes les routes IPv4 et IPv6 devant
être présentes dans la table de routage de l'hôte cible.

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant décrit une route par défaut via le routeur
    interne mutualisé de Rossetti. La passerelle à utiliser est résolue
    dynamiquement par Ansible en filtrant la liste des passerelles IP
    (variable [``my_network_gateways``][my_network_gateways]) afin de
    cibler celle portant l'étiquette ``default``

    ````yaml
    # host_vars/<ansible_host>/my/network/routes/_default.yml:
    ---
    my_network_routes__default:
      - uid: default
        tags: ['inet4', 'default']
        description: Default route via internal gateway
        destination: '0.0.0.0/0'
        gateway: >-
          {{
            my_network_routing_gateways |
            selectattr('tags', 'contains', 'default') | first
          }}
    ````

    Une fois la fusion des variables partielles réalisée et les
    références résolues, l'on obtiendra la variable consolidée
    ``my_network_routes`` suivante:

    ````yaml
    ---
    my_network_gateways:
      - uid: default
        tags: ['inet4', 'default']
        description: Default route via internal gateway
        destination: '0.0.0.0/0'
        gateway:
          uid: ros-mtl-gwi-00
          tags: ['default']
          description: Internal gateway
          addresses:
            inet4: '10.6.100.254'
    ````
