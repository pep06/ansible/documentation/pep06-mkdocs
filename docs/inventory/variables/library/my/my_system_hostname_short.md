---
title: system_hostname_short  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# ``my_system_hostname_short`` (**str**, _obligatoire_)

## Description

Cette variable stocke la forme courte du nom d'hôte associé à un hôte
Ansible. Sa présence est obligatoire car elle est utilisée par Ansible
pour générer la chaîne de connexion SSH.

## Type

Cette variable accepte une valeur de type **chaîne** (``str``).

## Contraintes

Cette variable accepte une chaîne de caractères arbitraire, qui doit
seulement être constituée de caractères alphanumériques, underscore ou
de tirets (``^[a-zA-Z0-9_\-]+$``).

## Exemples

???+ example "Exemple 1"
    L'extrait de code suivant définit la chaîne ``mylocalhost`` comme
    nom d'hôte court:

    ````yaml
    ---
    my_system_hostname_short: mylocalhost
    ````
