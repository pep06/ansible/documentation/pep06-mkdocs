---
title: network_interfaces(__.*)?  
author:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[network.interface]: /types/library/fr.pep06.schemas/network.interface/

# ``my_network_interfaces(__.*)?`` (**network.interface[]**, _optionnel_, _partiel_)

???+ info "Variable consolidée"
    La variable d'inventaire ``my_network_interfaces`` est générée
    dynamiquement par Ansible à partir des variables partielles dont le
    nom débute par ``my_network_interfaces__``.

## Description

Cette variable stocke la liste des interfaces réseau attribuées à un
hôte Ansible.

## Type

Cette variable est de type liste. Chaque entrée de la liste est un
dictionnaire (``dict``), correspondant à une instance du type de données
[``fr.pep06.schemas.network.interface``][network.interface].

??? warning "Migration en cours"
    Historiquement, les éléments de cette liste furent définis sans
    formalisme particulier. Ces éléments sont en cours de migration
    vers le type [``network.interface``][network.interface]. Par
    conséquent, les instances actuellement présentes dans l'inventaire
    comportent des clés additionnelles qui ne sont pas définies par
    le type [``network.interface``][network.interface]. Ces clés seront
    supprimées au fil de l'avancée de la migration.

## Usage

Cette liste contient habituellement la liste de toutes les interfaces
réseau utilisables par l'hôte configuré.

## Exemples

Cette variable contenant des entrées en cours de migration vers un
nouveau type de données, il est impossible de fournir un exemple
stable d'implémentation pour le moment.
