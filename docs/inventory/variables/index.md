---
title: Vue d'ensemble  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[types]: /types
[gitlab]: https://github.com/geerlingguy/ansible-role-gitlab
[pillar]: https://docs.saltstack.com/en/latest/topics/tutorials/pillar.html

# Vue d'ensemble des variables d'inventaire

Ansible permet la définition de variables d'inventaire, qui constituent
un moyen de représenter diverses informations à propos d'un hôte, d'un
groupe d'hôtes, ou de n'importe quoi d'autre. Chaque inventaire Ansible
peut contenir un nombre illimité de ces variables, qui pourront être
consommées par les tâches, rôles et playbooks lors des tâches de
configuration.

Parce que les variables d'inventaire sont faciles à utiliser, elles ont
tendance à se multiplier rapidement et à devenir très compliquées à
maintenir sur le long terme. Pour éviter ce genre d'écueils, il a été
décidé de conditionner l'usage de ces variables à leur documentation.
La présente section est le fruit de cet effort.

## Catégories de variables
Nos inventaires Ansible peuvent contenir quatre types de variables:

  1. **Métavariables**  
     Les métavariables sont des variables décrivant l'inventaire
     lui-même. Elle représentent divers aspects de l'inventaire, et son
     utilisées par certaines tâches afin de réaliser des opérations sur
     l'inventaire lui-même, avant l'exécution des playbooks, telles
     que la fusion des variables partielles. Les métavariables portent
     toutes un nom débutant par le préfixe ``meta_``.

  2. **Variables de configuration**  
     Les variables de configuration constituent le type le plus courant
     de variables d'inventaire. Elles sont utilisées pour représenter
     divers aspects de la configuration d'un hôte, et portent toutes
     un nom débutant par le préfixe ``my_``.
  
  3. **Variables communes**  
     Les variables communes sont associées au groupe d'hôtes spécial
     ``all``, ce qui les rend accessibles à la totalité des hôtes de
     l'inventaire. Ces variables portent un nom débutant par le préfixe
     ``our_``.

  4. **Variables systèmes**  
     Les variables systèmes définissent divers aspects de la
     configuration du moteur Ansible lui-même. Elles emploient des noms
     remarquables débutant par le préfixe ``ansible_``, et sont
     maintenues par l'équipe de développement d'Ansible.

## Nommage des variables
Les noms des variables d'inventaires sont constitués d'une succession
de mots séparés les uns des autres par un ou plusieurs caractères
underscore (``_``). Chacun de ces mots est appelé un _segment_

??? example "Exemple de division d'un nom de variable en segments"
    La variable d'inventaire ``my_network_interfaces`` est constituée
    de trois segments: ``my``, ``network`` et ``interfaces``.

### Préfixe
Le premier segment du nom d'une variable est appelé _préfixe_. Ce
préfixe indique la catégorie à laquelle la variable appartient. Les
variables d'inventaire peuvent comporter les préfixes suivants:

| Préfixe      | Catégorie                  |
| ------------ | -------------------------- |
| ``ansible_`` | Variables systèmes         |
| ``meta_``    | Métavariables              |
| ``my_``      | Variables de configuration |
| ``our_``     | Variables communes         |

Ces catégories sont décrites dans la section précédente. L'utilisation
d'autres préfixes que ceux décrits ci-dessus est **interdite**.

??? example "Exemple de préfixe de variable"
    Le préfixe ``my`` dans la variable d'inventaire
    ``my_network_interfaces`` nous indique qu'il s'agit d'une variable
    de configuration.

### Suffixe
Les noms de certaines variables peuvent inclure un ou plusieurs segments
terminaux séparés du reste du nom par une séquence de deux underscores
(``__``). Les segments apparaissant après cette séquence forment une
structure appelée _suffixe_. La présence d'un suffixe dans le nom d'une
variable indique que celle-ci est _partielle_. Les variables partielles
sont décrites plus bas, dans une section dédiée.

??? example "Exemple de variable portant un suffixe"
    La variable d'inventaire ``my_network_interfaces__wan0``porte le
    suffixe ``wan0``.

### Infixe
On appelle _infixe_ ce qui reste du nom d'une variable après qu'il ait
été amputé de son préfixe et de son éventuel suffixe. Il s'agit de la
partie centrale du nom d'une variable.

??? example "Exemple d'infixe"
    L'infixe de la variable d'inventaire ``my_network_interfaces__wan0``
    est ``network_interfaces``.

### Ordre des segments
L'ordre d'apparition des segments dans les noms de variables est employé
comme un système minimaliste d'espaces de noms. Cela signifie que les
segments sont sémantiquement liés par des relation parent-enfant se
déroulant de la gauche vers la droite. La signification d'un segment
doit par conséquent toujours être envisagée dans le contexte sémantique
de son antécédent, comme en anglais naturel.

??? example "Illustration l'incidence de l'ordre des segments"
    Reprenons notre variable d'inventaire ``my_network_interfaces``.
    Dans le nom de cette variable, l'ordre d'apparition des segments
    a une importance, puisqu'il nous permet de déduire que la variable
    représente une information à propos de la configuration d'un hôte
    (``my_``), et plus précisément des interfaces (``interfaces``) de
    son sous-système réseau (``network``).

### Anglais naturel
Outre l'attention portée à l'ordre des segments, les noms de variables
emploient plusieurs autres conventions visant à les rapprocher le
plus possible de dénominations en anglais naturel. Ce parti-pris
permet aux utilisateurs de déduire intuitivement le type de données
associé à une variable, et ainsi que prévenir les erreurs de types.
Le tableau ci-dessous décrit ces conventions:

| Type de variable | Convention en anglais naturel                                         |
| ---------------- | --------------------------------------------------------------------- |
| Scalaire         | Le segment final de l'infixe est un nom au singulier                  |
| Booléen          | Le segment final de l'infixe est un participe passé                   |
| Liste            | Le segment final de l'infixe est un nom au pluriel                    |
| Dictionnaire     | L'infixe se termine par un nom au pluriel suivi de ``_by_<key_name>`` |
| Complexe         | Le segment final de l'infixe est ``data``                             |

L'usage de ces règles est **obligatoire**, et ne souffre aucune
exception. Notez que ces conventions s'appliquent toujours au dernier
segment d'un infixe, les suffixes n'étant pas considérés comme
sémantiquement incidents.

??? example "Illustration des conventions d'anglais naturel"
    Voici quelques exemples de noms de variables d'inventaires montrant
    comment l'usage de l'anglais naturel permet la déduction du type
    d'une variable:

    - ``my_network_dns_suffix`` référence une valeur scalaire (``str``).

    - ``my_network_dns_servers`` référence une liste (``str[]``).

    - ``my_network_routing_forwarding_enabled`` référence une valeur
      boléenne (``bool``).

    - ``meta_partial_list_vars_by_name`` référence une valeur de type
      dictionnaire (``dict``) dont chaque entrée associé un nom
      (``by_name``) à une variable partielle de liste
      (``partial_list_vars``).

    - ``my_network_framework_data`` référence une valeur de type
      complexe (``dict``).

## Déclaration
Les variables d'inventaires sont déclarées dans des fichiers YAML
localisées dans les sous-répertoires ``host_vars`` et ``group_vars``
d'un inventaire.

### Organisation des fichiers
Les fichiers de variables sont organisés en une hiérarchie de sous-
répertoires. Chaque niveau de la hiérarchie correspond à un des segments
de l'infixe, à partir duquel le répertoire est nommé. Le fichier YAML,
qui correspond à l'élément feuille, porte le même nom que le dernier
segment de l'infixe.

??? example "Illustration de ces règles"
    La variable d'inventaire ``my_network_framework_data`` sera déclarée
    dans un fichier YAML localisé à l'emplacement
    ``my/network/framework/data.yml``, dans le sous-répertoire
    ``host_vars/<host>/`` ou ``group_vars/<group>/`` de l'inventaire.

### Déclarations multiples
Bien qu'un fichier YAML permette de déclarer plusieurs variables à la
fois, les règles d'organisation que nous venons d'exposer impliquent que
**chaque fichier YAML ne peut contenir qu'une seule déclaration de
variable**. Cette règle améliore la granularité des données et diminue
le risque de conflits dans les systèmes de contrôle de versions.

### Commentaires
Bien que YAML autorise la présence de commentaires, ces-derniers doivent
être employés avec parcimonie: les noms de variables devraient toujours
être suffisamment explicites pour éviter le recours à des indications
supplémentaires. Les commentaires doivent être réservés aux cas
particuliers.

## Variables spéciales
### Variables secrètes
### Variables partielles
