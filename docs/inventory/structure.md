---
title: Structure  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[ansible_group_priority]: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#how-variables-are-merged

# Structure des inventaires Ansible

Cette page décrit la structure des inventaires Ansible maintenus par
l'association.

???+ info "Nos inventaires utilisent le format YAML"
    Tous nos inventaires emploient le format YAML. L'usage du format INI
    a été abandonné car les inventaires devenaient difficilement
    lisibles au-delà d'une certaine taille.

Chaque inventaire comprend un fichier ``hosts.yaml``, situé à la racine
du dépôt, qui contient la liste des hôtes et groupes d'hôtes définis
par l'inventaire. La racine du dépôt contient également deux
sous-répertoires ``host_vars`` et ``group_vars``, qui sont dédiés au
stockage des fichiers YAML contenant les variables d'inventaire.

## Fichier ``hosts.yml``

## Sous-répertoire ``host_vars``
Ce répertoire contient un sous-répertoire par hôte défini dans
l'inventaire. Lors du chargement de l'inventaire, Ansible énumère tous
les hôtes connus, et vérifie pour chacun s'il existe un sous-répertoire
de ``host_vars`` portant le même nom. Lorsqu'un tel sous-répertoire est
trouvé, Ansible scanne son contenu de manière récursive à la recherche
de fichiers YAML contenant des déclarations de variables.

Ainsi, toute variable déclarée dans un fichier YAML stocké dans un
sous-répertoire de ``host_vars`` sera automatiquement rendue disponible
pour l'hôte de même nom.

???+ example "Exemple de variables d'inventaire définies via ``host_vars``"
    Si l'inventaire définit un hôte ``ros.ass.core.ros-ass-bkp-01``,
    le fichier YAML ``host_vars/my/sys/users.yml`` sera automatiquement
    inclus par Ansible, et la variable qu'il contient rendue disponible:
    ````
    /
    +-- inventories/
        +-- pep06-4.1801/
            +-- group_vars/
            +-- host_vars/
            |   +-- ros.ass.core.ros-ass-bkp-01/
            |       +-- my/
            |           +-- sys/
            |               +-- users.yml
            +-- hosts.yml
    ````
    ````sh
    $ cat inventories/pep06-4.1801/host_vars/ros.ass.core.ros-ass-bkp-01/my/sys/users.yml
    my_sys_users = {}
    ````
    ````sh
    $ ansible -i inventories/pep06-4.1801 -m debug -a "var=my_sys_users" ros.ass.core.ros-ass-bkp-01
    ros.ass.core.ros-ass-bkp-01 | success >> {
        "my_sys_users": {}
    }
    ````

## Sous-répertoire ``group_vars``
Ce répertoire est l'équivalent de ``host_vars`` pour les groupes
d'hôtes: il permet d'affecter des variables d'inventaire à tous les
hôtes membres d'un groupe. Lors du chargement de l'inventaire, Ansible
énumère tous les groupes connus, et vérifie pour chacun s'il existe un
sous-répertoire de ``group_vars`` portant le même nom. Lorsqu'un tel
sous-répertoire est trouvé, Ansible scanne son contenu de manière
récursive à la recherche de fichiers YAML contenant des déclarations de
varaibles.

Ainsi, toute variable déclarée dans un fichier YAML stocké dans un
sous-répertoire de ``group_vars`` sera automatiquement rendue disponible
pour tous les hôtes membres de groupe.

???+ note "Précédence des variables d'hôtes"
    Lors du chargement des variables, le répertoire ``group_vars`` est
    traité **avant** le répertoire ``host_vars``: ceci garantit que dans
    l'hypothèse où une variable serait définie à la fois dans
    ``group_vars`` et ``host_vars``, la valeur issue de ``host_vars``
    sera prioritaire. Ce mécanisme permet à un hôte de facilement
    surcharger une variable d'inventaire.

    De la même façon, lors du chargement des variables issues de
    ``group_vars``, celles affectées au groupe spécial ``all`` seront
    traitées avant celles des autres groupes, ce qui permet de définir
    des valeurs par défaut surchargeables par des groupes particuliers.

    Notez que si l'ordre de traitement des groupes différents de ``all``
    est [configurable][ansible_group_priority], nous avons jusqu'ici
    réussi à éviter d'y avoir recours et espérons que cela continue.
