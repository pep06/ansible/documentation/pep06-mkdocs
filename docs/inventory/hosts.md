---
title: Hôtes
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

# À propos des hôtes de l'inventaire

## Identifiants d'hôtes
Tous les hôtes déclarés dans l'inventaire doivent l'être au moyen d'un
**identifiant unique**. Dans l'absolu, cet identifiant est libre,
indépendant du nom d'hôte, et laissé à la discrétion de chaque
utilisateur d'Ansible. Mais dans les inventaires que nous maintenons,
les identifiants d'hôtes sont construits à partir de règles précises
que nous exposons ici.

### Caractères autorisés
Les identifiants d'hôtes peuvent uniquement être composés de caractères
alphanumériques non-spéciaux, ainsi que des caractères point (``.``),
tiret (``-``) et underscore (``_``). À ces contraintes, nous ajoutons
l'interdiction d'emploi des lettres majuscules.

### Construction
Par convention, chaque hôte déclaré dans l'inventaire doit porter un
identifiant construit à partir du patron suivant:

````
<reversed_dns_infix>.<short_hostname>
````

Où:

  - ``<reversed_dns_infix>`` correspond au suffixe DNS de l'hôte amputé
    du suffixe organisationnel et exprimé en ordre inverse.

  - ``<host_name>`` correspond à la version courte (sans suffixe DNS)
    de son nom d'hôte.

L'on entend par _suffixe organisationnel_ le suffixe DNS correspondant
au domaine d'administration de l'hôte, tel que ``.intranet.pep06.fr``
pour l'organisation PEP06. Chaque inventaire étant censé n'être lié qu'à
une seule organisation, la présence du suffixe organisationnel est
superflue. Son amputation permet d'alléger les identifiants d'hôtes.

???+ example "Exemple de construction d'un identifiant d'hôte"
    L'hôte ``ros-ass-bkp-01.core.ass.ros.intranet.pep06.fr`` sera connu
    sous l'identifiant ``ros.ass.core.ros-ass-bkp-01``, car:

      - Son suffixe DNS ``core.ass.ros.intranet.pep06.fr``, une fois
        amputé du suffixe organisationnel ``.intranet.pep06.fr``, donne
        l'infixe DNS ``ros.ass.core``
      - Cet infixe est ensuite renversé pour donner ``ros.ass.core``.
      - Enfin, le nom d'hôte court est ajouté à sa suite pour donner
        l'identifiant d'inventaire ``ros.ass.core.ros-ass-bkp-01``.

### Usage
L'emploi de cette convention de nommage offre plusieurs avantages:

  - Les sous-répertoires de ``host_vars`` sont d'office groupés par
    suffixe DNS, ce qui aux PEP06 correspond à un groupement par
    sous-réseau.

  - Les hôtes sont de fait aisément filtrables par suffixe DNS, ce qui
    correspond aux PEP06 à un filtrage par sous-réseau.

    ???+ example "Exemples de filtrage par identifiants d'hôtes"
        - Tous les hôtes de Rossetti: ``ros.*``
        - Tous les serveurs du centre de santé: ``ros.cds.core.*``
        - Tous les clients administratifs filaires de l'association:
          ``*.*.admin.wired.*``

  - Des hôtes portant des noms courts identiques peuvent être déclarés
    sans risque de collision tant que leur suffixe DNS varie.
