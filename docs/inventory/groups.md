---
title: Groupes
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06
language: fr-FR
---

[group_categories]: #categories-de-groupes

# À propos des groupes de l'inventaire

En plus de recenser des hôtes, l'inventaire permet en outre de les
regrouper au sein de **groupes d'hôtes**. Cette page recense les
conventions relatives à ces groupes.

## Catégories de groupes
Bien qu'Ansible permette de définir des groupes arbitraires, nos
inventaires requièrent que chacun de leurs groupes appartienne à l'une
des catégories suivantes, indiquée par le préfixe de son identifiant:

| Préfixe          | Critère de regroupement           |
| ---------------- | --------------------------------- |
| ``host_class__`` | Classe d'hôte                     |
| ``platform__``   | Famille de système d'exploitation |
| ``subnet__``     | Sous-réseau                       |

## Identifiants de groupes
De la même manière que pour les hôtes, chaque groupe d'inventaire doit
porter un identifiant unique. Cet identifiant est formé sur la base
des règles de construction présentées dans cette section.

### Notions préliminaires
#### Segment
Chaque identifiant de groupe d'inventaire est constitué d'un nombre
variable de segments, délimités par le caractère underscore (``_``).
Cette structuration est identique à celle des variables.

???+ example "Exemple d'identifiant de groupe délimité en segments"
    Le groupe d'inventaire ``subnet__ros_ass_core`` est constitué de
    4 segments: ``subnet``, ``ros``, ``ass`` et ``core``.

#### Préfixe
Le premier segment d'un identifiant de groupe d'inventaire est appelé
_préfixe_. Il est séparé du reste des segments par deux caractères
underscore (``__``) et indique la [catégorie][group_categories] à
laquelle le groupe appartient. Seuls les préfixes définis dans le
tableau recensant les [catégories de groupes][group_categories] sont
autorisés.

???+ example "Exemple de préfixe d'identifiant de groupe d'inventaire"
    Le groupe d'inventaire ``subnet__ros_ass_core`` possède le préfixe
    ``subnet__``, ce qui indique qu'il regroupe des hôtes appartenant
    au même sous-réseau.

#### Infixe
Le reste des segments constituant un identifiant de groupe d'inventaire
est appelé _infixe_. Les règles qui régissent l'infixe dépendent de la
catégorie du groupe concerné et seront exposées plus loin.

???+ example "Exemple d'infixe d'identifiant de groupe d'inventaire"
    L'infixe du groupe d'inventaire ``subnet__ros_ass_core`` est
    ``ros_ass_core``.

### Caractères autorisés
Les identifiants des groupes d'inventaires ne peuvent contenir que des
caractères alphanumériques non-spéciaux ou le caractère underscore
(``_``). L'emploi de lettres majuscules est supporté par Ansible mais
interdit par nos conventions. Tout identifiant de groupe doit par
conséquent valider l'expression régulière suivante, qui inclut
l'obligation de préfixation:

````perl
/^[a-z][a-z0-9]+_(_[a-z0-9]+)+$/
````
