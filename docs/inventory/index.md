---
title: Vue d'ensemble  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>  
affiliation: PEP06  
language: fr-FR  
---

[netbox]: https://netbox.readthedocs.io/en/stable/
[repo-pep06_3.1511]: https://bitbucket.org/pep06/ansible-inventory-pep06-3.1511
[repo-pep06_4.1801]: https://bitbucket.org/pep06/ansible-inventory-pep06-4.1801
[roles]:     /roles
[variables]: ./variables/

# Vue d'ensemble des inventaires Ansible

## Introduction
Pour cibler les hôtes à configurer et obtenir leurs caractéristiques,
Ansible s'appuie sur un **inventaire**. Cet inventaire constitue une
sorte de base de données contenant:

  - **La liste complète des hôtes connus d'Ansible**  
    Tout hôte doit être déclaré dans l'inventaire afin qu'Ansible ne
    puisse s'y connecter.

  - **La répartition des hôtes en différents groupes**  
    L'inventaire permet de grouper les hôtes, ce qui permet ensuite de
    cibler tous les hôtes d'un même site ou de même rôle.

  - **Les variables d'inventaire**  
    Les hôtes ou groupes d'hôtes peuvent se voir assigner un nombre
    illimité de variables, décrivant de manière libre divers aspects
    de leur configuration. Ces variables peuvent ensuite utilisées
    par les [rôles][roles] pour adapter leurs actions aux particularités
    des nœuds configurés.

En théorie, les inventaires Ansible sont indépendants des rôles,
playbooks et listes de tâches utilisés pour configurer les hôtes.
Dans la réalité, cette indépendance n'est possible qu'à condition de
suivre des règles strictes _imposant_ une séparation entre les espaces
de noms des inventaires et du reste des données Ansible. C'est le choix
que nous avons fait, et qui est l'objet de cette documentation.

## Dépôts d'inventaires
L'association utilise actuellement deux inventaires Ansibles distincts,
afin de séparer les hôtes encore soumis à la révision ``3.1511`` du
plan d'adressage IP de ceux déjà migrés vers la révision ``4.1801``.
Chaque inventaire est hébergé dans un dépôt individuel dont le nom est
construit sur le patron ``ansible-inventory-<uid>``, où:

  - ``uid`` est un identifiant unique correspondant généralement au
    nom de l'entité inventoriée.

Le tableau ci-dessous dresse la liste des inventaires actuellement
disponibles:

| Chemin relatif à la racine d'un dépôt de playbooks | Dépôt Bitbucket                                         |
| -------------------------------------------------- | ------------------------------------------------------- |
| ``/inventories/pep06-3.1511``                      | [``ansible-inventory-pep06_3.1511``][repo-pep06_3.1511] |
| ``/inventories/pep06-4.1801``                      | [``ansible-inventory-pep06_4.1801``][repo-pep06_4.1801] |

Les dépôts d'inventaires sont stockés dans le sous-répertoire
``inventories/`` du dépôt principal, dont ils constituent des
sous-modules.

## Formats d'inventaires
Les inventaires utilisés par l'association sont tous statiques.
La migration vers des inventaires dynamiques est envisagée à moyen
terme, une fois que nous aurons pu migrer notre gestion IPAM vers
[Netbox][netbox], qui est officiellement supporté par le projet Ansible
comme source d'inventaire.

Tous les inventaires maintenus par l'association emploient le format
YAML. L'usage du format INI a été abandonné car les inventaires
devenaient difficilement lisibles au-delà d'une certaine taille.

## Variables
Nous faisons un usage tellement intensif des variables d'inventaires
qu'elles ont droit à [leur propre page de documentation][variables],
que nous vous invitons à consulter!
