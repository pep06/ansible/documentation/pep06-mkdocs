---
title: Variables  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[hash_behaviour]: https://docs.ansible.com/ansible/latest/reference_appendices/config.html#default-hash-behaviour
[inventory_variables]: /inventory/variables/
[partial_vars]: #variables-partielles
[types]: /types
[variable_categories]: #categories-de-variables
[var_cat_meta]: #metavariables-dinventaires

# À propos des variables Ansible

Cette page tente de dresser un tableau d'ensemble des conventions
relatives à l'utilisation des variables Ansible. Des pages plus précises
précises ciblant des types de variables spécifiques sont disponibles
dans d'autres sections de la documentation, notamment concernant:

  - Les [variables d'inventaire][inventory_variables]

## Introduction
L'utilisation des variables Ansible obéit à des règles propres aux
PEP06, qui ont pour but de faciliter leur gestion à moyenne et grande
échelle. Ces règles régissent à la fois le nommage, le contenu et
les modalités de définition de ces variables.

## Catégories de variables
Ansible n'implémente pas de mécanisme d'espace de noms: la totalité des
variables est définie dans un espace de noms global, ce qui laisse la
porte ouverte aux collisions. Pour contrer cette insuffisance,
nous avons introduit un système de catégorisation des variables, qui
assigne chaque variable Ansible à une des catégories suivantes:

| Catégorie                       | Préfixe      | Usage                                                        |
| ------------------------------- | ------------ | ------------------------------------------------------------ |
| **Variables systèmes**          | ``ansible_`` | Variables générées automatiquement par Ansible               |
| **Métavariables d'inventaires** | ``meta_``    | Variables décrivant des propriétés des inventaires eux-mêmes |
| **Variables d'inventaires**     | ``my_``      | Variables définies définies par les inventaires              |
| **Variables d'organisations**   | ``org_``     | Variables issues de données d'organisations                  |
| **Variables de rôles**          | ``role_``    | Variables définies ou attendues par les rôles                |

Toute variable utilisée avec Ansible doit appartenir à une de ces
catégories, et nommée en intégrant le préfixe qui lui est associée.

### Variables systèmes
Les **variables systèmes** correspondent aux variables générées
automatiquement par le moteur Ansible, notamment lors de la collecte
de faits concernant un hôte distant. Ansible préfixe lui-même ces
variables afin de les rendre reconnaissables.

???+ example "Exemples de variables systèmes"
    Les variables ``ansible_hostname`` ou ``ansible_distribution`` sont
    des variables systèmes définies automatiquement par Ansible.

### Métavariables d'inventaires
Les **métavariables d'inventaires** sont des variables définies dans un
inventaire et décrivant l'inventaire lui-même. Ces variables sont rares,
et sont notamment utiles pour exécuter des listes de tâches ayant besoin
d'une connaissance réflexive de l'inventaire lui-même, telles que la
fusion de variables partielles.

???+ example "Exemple de métavariable d'inventaire"
    La variable ``meta_partial_list_vars_by_name`` contient un
    dictionnaire (``dict``) identifiant les variables d'inventaire
    partielles de type liste éligibles à la fusion. Cette variable est
    requise afin que le plugin réalisant l'opération de fusion puisse
    savoir quelle variables fusionner et sous quel nom.

### Variables d'inventaires
Les **variables d'inventaires** sont des variables définies dans un
inventaire et décrivant un aspect de la configuration d'un hôte ou d'un
groupe d'hôtes. Le contexte des tâches Ansible étant toujours celui d'un
hôte unique, l'usage du préfixe _my_ semblait naturel pour refléter la
relation d'appartenance liant la variable à son hôte.

???+ example "Exemple de variable d'inventaire"
    La variable ``my_sys_net_filter_rules`` est une variable
    d'inventaire contenant une liste de règles de pare-feu. Lors de
    l'exécution d'Ansible sur un hôte distant, le contenu de cette
    variable sera utilisé pour adapter la configuration du pare-feu
    local.

L'extrême majorité des variables définies dans un inventaire appartient
à la catégorie des variables d'inventaires, l'autre possibilité étant
qu'il s'agisse de [métavariables d'inventaire][var_cat_meta].

### Variables d'organisations
Les **variables d'organisations** sont des variables définies par une
source de données externe à l'inventaire, et décrivant divers aspects
d'une organisation.

???+ example "Exemple de variable d'organisation"
    La variable ``org_pep06_hr_positions`` référence un
    dictionnaire contenant le détail des professions présentes dans
    l'organisation PEP06. Cette variable n'est pas définie dans un
    inventaire, mais dans un dépôt externe de données d'organisation.

### Variables de rôles
Les **variables de rôles** sont des variables définies ou attendues
par les rôles Ansible. Chaque rôle est tenu de préfixer ses variables
par un préfixe unique qui lui est assigné. Ce préfixe débute toujours
par ``role_``.

???+ example "Exemple de variable de rôle"
    La variable ``role_backup_restic_params`` contient les paramètres
    d'invocation du rôle backup/restic.

## Nommage des variables
Les noms des variables Ansible doivent respecter des règles strictes
qui sont exposées dans cette section.

### Notions préliminaires
Le nom d'une variable Ansible est composé d'un nombre libre de
_segments_, délimités par le caractère underscore (``_``).

???+ example "Exemple de découpage d'un nom de variable en segments"
    La variable ``my_sys_users`` comporte trois segments:
    ``my``, ``sys`` et ``users``

#### Préfixe
Le premier segment d'un nom de variable est appelé _préfixe_. Le préfixe
indique la [catégorie][variable_categories] à laquelle appartient la
variable.

???+ example "Exemple de préfixe"
    La variable ``my_sys_users`` comporte le préfixe ``my``, qui
    l'identifie comme une variable d'inventaire.

#### Suffixe
Certains noms de variables comprennent un segment final séparé des
autres par un double underscore (``__``). Ce segment final est appelé
_suffixe_, et indique que la variable est [partielle][partial_vars].

???+ example "Exemple de variable partielle"
    La variable ``my_sys_users__mysql`` comprend le suffixe ``mysql``,
    qui indique qu'il s'agit d'une variable partielle.

#### Infixe
L'ensemble des segments restants une fois exclus le préfixe et le
suffixe forme l'_infixe_, qui indique le [type][types] de la variable.

???+ example "Exemple d'infixe"
    La variable ``my_sys_users__mysql`` a pour infixe ``sys_users``,
    qui indique qu'elle référence des données de type ``sys.users``.

### Caractères autorisés
Tout nom de variable Ansible doit passer la validation par l'expression
régulière suivante:

````perl
/^[a-z][a-z0-9]*(_[a-z0-9]+)+(__[a-z0-9]+)?$/
````

En d'autres termes, pour les non-initiés:

  - Les noms de variables peuvent uniquement contenir des caractères
    alphanumériques minuscules non-spéciaux et le signe underscore
    (``_``). L'usage du tiret (``-``) est interdit en raison de son
    ambiguité en contexte YAML. L'usage des lettres majuscules est
    également prohibé.

  - Les noms de variables doivent obligatoirement débuter par une
    lettre.

  - Le signe underscore (``_``) est utilisé comme séparateur de
    segments.

  - L'usage du double underscore (``__``) n'est autorisé qu'en
    fin de variable, afin d'indiquer une variable partielle.

### Préfixation
Comme indiqué plus haut dans la section concernant les catégories de
variables, les noms de toutes les variables Ansible doivent comporter
le préfixe correspondant à leur catégorie.

La préfixation des noms de variables est obligatoire, sauf dans deux
contextes suivants, où le recours à des variables non-préfixées est
non-seulement possible mais conseillé:

  1. **À l'intérieur d'une énumération**  
    Dans le contexte d'une boucle de type ``with_{dict, items}``,
    la variable utilisée pour déréférencer l'objet courant n'a pas
    besoin d'être préfixée.

    ???+ example "Exemple de variable locale non-préfixée dans une énumération"
        Dans l'énumération suivante, ``user`` n'a qu'une portée locale
        et une durée de vie temporaire. Elle n'a donc pas besoin d'être
        préfixée:

        ```yaml
        - name: Ensure local users are present
          user:
            name: "{{ user.key }}"
            uid: "{{ user.value.uid }}"
            state: present
          with_dict: "{{ my_sys_users }}"
          loop_control:
            loop_var: user
        ```

  2. **À l'intérieur d'un modèle Jinja2**  
    Les variables définies à l'intérieur des modèles Jinja2 sont
    toujours de portée locale, et n'ont pas besoin d'être préfixées.

    ???+ example "Exemple de variable locale non-préfixée dans un modèle Jinja2"
        Dans le modèle suivant, les variables ``user_name`` et
        ``user_data`` n'ont pas besoin d'être préfixées.

        ```jinja
        {%- for user_name,user_data in my_sys_users.items() -%}
          Hello {{ user_name }}! Your uid is: {{ user_data.uid }}.
        {%- endfor -%}
        ```

### Infixes
La construction de l'_infixe_, c'est-à-dire des segments centraux des
noms de variables, obéit à des règles différentes selon la catégorie
à laquelle appartient la variable. Ces règles spécifiques sont exposées
dans les sections spécialisées de la documentation.

### Anglais naturel
Les noms de variables doivent employer la langue anglaise. Si vous
vous trouvez dans une situation où vous manquez de vocabulaire, ouvrez
un dictionnaire; ce sera l'occasion d'apprendre!

En outre, nous appliquons une méthode de nommage des variables qui vise
à utiliser des tournures linguistiques les plus naturelles possibles,
le but étant de concilier rigueur et approche intuitive des données.
L'usage de l'anglais naturel implique notamment les points suivants:

  - Les variables dont le type est un **booléen** doivent être nommées
    au moyen d'un **participe passé**.

    ??? note "Participes passés et booléens"
        Il faut garder à l'esprit que les variables Ansible ne décrivent
        pas des _actions_ mais des _états_. On ne veut pas _activer_ un
        pare-feu, mais _qu'il soit activé_. On emploie donc la voie
        passive, qui se traduit en anglais comme en français par l'usage
        de participes passés. En YAML, on traduira cela par une variable
        ``enabled`` (ou ``is_enabled``, si l'on y tient):
        ````yaml
        my_net_filter_config:
          enabled: true
        ````
        En revanche, la variable suivante n'est pas acceptable, parce
        qu'elle décrit une action au lieu d'un état:
        ````yaml
        my_net_filter_config:
          enable: true            # ARGH!
        ````

  - Les noms de variables référençant une **collection d'instances**
    sous la forme d'une **liste** doivent se terminer par un nom au
    **pluriel**.

    ??? note "Instances et collections d'instances"
        Par _liste_ on entend une liste au sens du type Python
        (``my_var: List[object] = []``) ou JSON/YAML (``my_var: []``).

        Cette règle du pluriel est très importante car elle permet de
        distinguer facilement les variables référençant _une instance
        particulière_ d'un type de données, comme une règle de pare-feu
        ou un utilisateur, de celles désignant des _collections_ de ces
        instances, telles qu'un jeu de règles de pare-feu ou une liste
        d'utilisateurs.

        Si je rencontre une variable nommée ``my_net_filter_rule__ssh``,
        je sais d'emblée qu'elle référence _une instance_ (et une seule)
        du type ``config.net.filter.rule``, qui décrit une règle de
        pare-feu. Je le sais parce que ``rule`` est au singulier. Le
        suffixe de la variable m'apprend également que cette règle
        concerne le service SSH.

        En revanche, si je rencontre une autre variable nommée
        ``my_net_filter_rules__ssh``, je saurai immédiatement qu'elle
        référence _une collection d'instances_ de type
        ``config.net.filter.rule``, c'est-à-dire une liste de règles
        de pare-feu.

        Cette distinction est fondamentale car Ansible requiert que
        l'on sache si les variables que l'on manipule sont scalaires
        ou itérables. En étant rigoureux quant au nommage des variables,
        on peut faire en sorte que cette information soit véhiculée par
        la variable elle-même!

  - Les noms de variables référençant une **collection d'instances**
    sous la forme d'un **dictionnaire** doivent se terminer par un nom
    au **pluriel** suivi de la mention ``_by_<k>``, où ``<k>`` est le
    nom de la clé utilisée pour l'indexation.

    ??? "Liste ou dictionnaire, il faut choisir"
        Bon, d'accord, ça a l'air compliqué, mais c'est en fait très
        simple. Parfois, il est plus commode de stocker une collection
        d'instances sous la forme d'un dictionnaire que d'une liste.

        Par exemple, si l'on souhaite stocker une liste d'utilisateurs,
        c'est-à-dire un jeu d'instances de type ``config.sys.user``,
        nous avons deux possibilités:

          1. Utiliser une liste. Dans ce cas, on créera une variable
             ``my_sys_users__blablabla``, qui ressemblera à ça:
             ````yaml
             my_sys_users__blablabla:
               - name: bob
                 uid: 1000
               - name: andrew
                 uid: 1001
             ...
             ````
             C'est le cas de figure discuté plus haut, relatif aux
             collections d'instances sous forme de listes.

          2. Utiliser un dictionnaire, qui offre l'avantage de permettre
             l'indexation des entrées par nom d'utilisateur: cela
             permet d'atteindre facilement une entrée pour peu que l'on
             connaisse le nom de l'utilisateur visé. En outre, Ansible
             vérifiera l'intégrité de notre dictionnaire et nous
             alertera s'il détecte la présence d'un doublon de clé.
             Cela donnerait:
             ````yaml
             my_sys_users_by_name__blablabla:
               bob:
                 uid: 1000
               andrew:
                 uid: 1001
             ...
             ````
             Dans ce cas, le nom de la variable intègre ``_by_name``,
             puique nous utilisons la valeur de la clé ``name`` pour
             indexer le dictionnaire. Nous aurions aussi bien pu indexer
             par ``uid``:
             ````yaml
             my_sys_users_by_uid__blablabla:
               1000:
                 name: bob
               1001:
                 name: andrew
             ...
             ````
             Notez que dans ces deux exemples, nous avons supprimé la
             clé utilisée pour l'indexation du corps des instances, par
             souci d'éviter les répétitions. Ce n'est toutefois pas une
             obligation et nous aurions aussi bien pu les conserver.

Pour résumer:

| Type de variable         | Modèle de nommage            | Exemples                                   |
| ------------------------ | ---------------------------- | ------------------------------------------ |
| Booléen                  | Participe passé              | ``enabled``, ``installed``, ``enforced``   |
| Instance individuelle    | Nom au singulier             | ``my_net_dns_suffix``, ``my_sys_hostname`` |
| Liste d'instances        | Nom au pluriel               | ``my_net_filter_rules``, ``my_sys_users``  |
| Dictionnaire d'instances | Nom au pluriel + ``_by_<k>`` | ``my_storage_volumes_by_dev``              |

## Variables partielles
L'une des limites d'Ansible est qu'il n'existe pas de mécanisme natif
permettant de fusionner le contenu de variables homonyme. Le paramètre
de configuration [hash_behaviour][hash_behaviour] pourrait permettre
de parvenir à ce résultat, mais ses répercussions sont trop larges pour
qu'il puisse être utilisé sans risques.

Par conséquent, nous avons implémenté notre propre mécanisme de fusion
de variables, qui emploie un plugin d'action pour générer des variables
consolidées à partir de variables dites _partielles_.

Une **variable partielle** est une variable dont le nom comporte un
suffixe en ``/__[a-z0-9]+/``, c'est-à-dire un double underscore suivi
d'un mot. Toute variable dont le nom est suffixé de la sorte sera
considérée comme _partielle_, c'est-à-dire susceptible de servir de
source à une opération de fusion. Cela ne change rien au comportement
de ces variables; il s'agit simplement d'une convention de nommage.

???+ example "Exemples de variables partielles"
    Prenons un hôte nommé ``core.ass.ros.ros-ass-dbmy-01``, et
    considérons que cet hôte est un serveur MySQL dont le pare-feu
    doit exposer le socket MySQL au réseau local. Notre hôte appartient
    à plusieurs groupes d'inventaire, dont les groupes
    ``platform__linux`` (tous les serveurs Linux) et ``hostclass__dbmy``
    (tous les serveurs MySQL).

    Le rôle chargé de configurer le pare-feu n'étant invoqué qu'une
    seule fois, il doit, au moment de son invocation, disposer d'une
    variable recensant la totalité des règles de pare-feu à configurer.

    Parmi ces règles, certaines sont communes à l'ensemble des serveurs
    Linux. C'est par exemple le cas de celles autorisant les connexions
    SSH ou l'accès aux serveurs de mises à jour. Ces règles-ci seront
    définies dans des variables liées au groupe ``platform__linux``,
    afin de s'appliquer à l'ensemble des serveurs Linux. Les règles
    spécifiques à MySQL seront quant à elles définies au niveau du
    groupe  ``hostclass__mysql``.

    Ansible étant incapable de fusionner les variables homonymes, si
    nous utilisons le même nom de variable dans chaque groupe, seule
    l'une des deux valeurs subsistera une fois l'inventaire chargé
    (celle des deux qui sera conservée dépend de la priorité associée à
    chaque groupe). Mais si nous utilisons des variables différentes,
    nous serons incapables de fournir une variable unique au rôle
    chargé de configurer le pare-feu.

    La solution que nous avons retenue consiste à utiliser deux
    variables partielles pour définir les règles de pare-feu:

      1. Une première variable sera définie au niveau du groupe
        ``platform__linux`` et contiendra les règles de pare-feu
        communes à tous les serveurs Linux. Cette variable sera nommée:
        ``my_sys_net_filter_rules__common``

      2. Une seconde variable ``my_sys_net_filter_rules__mysql`` sera
         définie au niveau du groupe ``hostclass__dbmy``, et contiendra
         les règles de pare-feu spécifiques à MySQL.

    Avant l'invocation du role chargé de configurer le pare-feu, nous
    invoquerons le plugin d'action _merge_vars_ afin qu'il génère une
    nouvelle variable ``my_sys_net_filter_rules`` à partir du contenu
    des deux variables partielles. C'est cette variable consolidée que
    nous fournirons au rôle chargé de configurer le pare-feu.

###
