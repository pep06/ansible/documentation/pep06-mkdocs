---
title: Accueil  
authors:
  - Marin Bernard <marin.bernard@pep06.fr>
affiliation: PEP06  
language: fr-FR  
---

[ansible]: https://docs.ansible.com/ansible/latest/index.html
[yaml]: https://fr.wikipedia.org/wiki/YAML
[json]: https://fr.wikipedia.org/wiki/JavaScript_Object_Notation
[jinja2]: http://jinja.palletsprojects.com/en/2.10.x/

# Bienvenue!

La présente documentation détaille la manière dont le moteur de gestion
de la configuration **Ansible** est utilisé au sein des PEP06. Elle
suppose que vous connaissiez déjà les bases du fonctionnement d'Ansible,
et que vous soyez familier des langages de balisage [YAML][yaml] et
[JSON][json], ainsi que du moteur de modèles [Jinja2][jinja2].
Consultez la [documentation d'Ansible][ansible] pour vous familiariser
avec l'outil si nécessaire.

## Pourquoi cette documentation
Ansible tient davantage du _framework_ que de la solution clés en main.
Si la souplesse de l'outil fait indéniablement partie de ses avantages,
les environnements collaboratifs ont besoin d'un cadre structurant,
fait de normes et de conventions, qu'Ansible ne fournit pas. Il revient
donc à chaque équipe d'édicter ses propres conventions, ce qui est
l'objet de ce recueil.
